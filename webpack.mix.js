const mix = require('laravel-mix');
const tailwindcss = require('tailwindcss');

require('mix-tailwindcss');
require('laravel-mix-criticalcss');
require('laravel-mix-polyfill');
/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

// Frontend
mix.webpackConfig(require('./webpack.config'));

let publicSide =
    mix.options({
            uglify: {
                uglifyOptions: {
                    compress: mix.inProduction(),
                }
            },
            processCssUrls: false,
            postCss: [ tailwindcss('./tailwind.config.js') ],
        })
        .js('resources/js/app.js', 'public/js')
        .sass('resources/scss/app.scss', 'public/css')
        .sourceMaps(false, 'source-map')
        .criticalCss({
            enabled: mix.inProduction(),
            paths: {
                base: process.env.MIX_APP_URL,
                templates: './resources/views/criticalcss/',
                suffix: ''
            },
            urls: [
                { url: '/', template: 'home' },
            ],
            options: {
                processCssUrls: false,
                minify: true,
            },
        })
        .version();

let admin =
    mix.sass('resources/scss/admin.scss', 'public/css')
        .js('resources/js/admin.js', 'public/js')
        .sourceMaps(false, 'source-map')
        .version();
