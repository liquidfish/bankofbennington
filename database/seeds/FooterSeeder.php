<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\Footer;

class FooterSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if(!Footer::first()) {
            DB::table('footer')->insert([
                'content' => 'COPYRIGHT © 2021 THE BANK OF BENNINGTON • PHONE: 802.442.8121 • BANK ROUTING NUMBER: 211672609 • PHONE BANKING; 800.216.1103',
            ]);
        }
    }
}
