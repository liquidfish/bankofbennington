<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class FooterLinksSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('footer_links')->insert([
            'title' => 'SecurLOCK – For Your Debit Card',
            'content' => 'The Smartphone App That Keeps Your Debit Cards Safe! The FREE SecurLOCK app gives you the ability to turn your debit card on or off in …',
            'button_url' => 'https://bankofbennington.com',
            'button_text' => 'Be Safe',
            'image' => '/img/svg/lock.svg',
            'active' => true
        ]);
    }
}
