<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\LoginLink;

class LoginLinkSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if(!LoginLink::first()) {
            DB::table('login_links')->insert([
                'button_text' => 'Personal Checking',
                'button_url' => 'https://olb.thebankofbennington.com/login/',
                'active' => true
            ],
            [
                'button_text' => 'Sign Up',
                'button_url' => 'https://olb.thebankofbennington.com/Enrollment/EnrollmentAdv.aspx',
                'active' => true,
                'is_sign_up' => true
            ]);
        }
    }
}
