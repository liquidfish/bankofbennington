<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class HomeCardsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('home_cards')->insert([
            'title' => 'Summer Project?',
            'image' => '',
            'content' => 'Are You Ready to Take on Your NEXT Project? If you have an urge to tackle some home improvements, or get organized by …',
            'button_text' => 'Apply Online',
            'button_url' => 'https://bankofbennington.com',
            'active' => true
        ]);
    }
}
