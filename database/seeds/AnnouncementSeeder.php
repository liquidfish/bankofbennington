<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\Announcement;

class AnnouncementSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if(!Announcement::first()) {
            DB::table('announcements')->insert([
                'message' => '',
                'enabled' => false
            ]);
        }
    }
}
