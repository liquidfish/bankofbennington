<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddPageRoles extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('page_permissions', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('page_id', false, true);
			$table->foreign('page_id')->references('id')->on('pages')->onDelete('cascade');
			$table->integer('permission_id', false ,true);
			$table->foreign('permission_id')->references('id')->on('permissions')->onDelete('cascade');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('page_permissions');
	}

}