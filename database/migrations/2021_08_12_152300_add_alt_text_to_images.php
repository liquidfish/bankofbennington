<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddAltTextToImages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('home_cards', function (Blueprint $table) {
            $table->string('alt_text')->nullable();
        });

        Schema::table('home_slides', function (Blueprint $table) {
            $table->string('alt_text')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('home_cards', function (Blueprint $table) {
            $table->dropColumn('alt_text');
        });

        Schema::table('home_slides', function (Blueprint $table) {
            $table->dropColumn('alt_text');
        });
    }
}
