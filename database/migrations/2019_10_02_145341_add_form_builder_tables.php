<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFormBuilderTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('forms',function(Blueprint $table){
            $table->increments('id');
            $table->string('title');
            $table->mediumText('columns');
            $table->string('submit_button_title', 100)->default('Submit');
            $table->text('description')->nullable();
            $table->text('notification_addresses')->nullable();
            $table->boolean('published')->default(false);
            $table->boolean('requires_receipt')->default(false);
            $table->string('receipt_email_field',10)->nullable();
            $table->boolean('requires_recaptcha')->default(false);
            $table->boolean('scheduled')->default(false);
            $table->string('success_title')->nullable();
            $table->text('success_description')->nullable();
            $table->text('closing_comments')->nullable();
            $table->date('start_date')->nullable();
            $table->date('end_date')->nullable();
            $table->boolean('terms')->default(false);
            $table->string('terms_agree')->nullable();
            $table->text('terms_conditions')->nullable();
            $table->timestamps();
        });
        Schema::create('form_entries',function(Blueprint $table){
            $table->bigIncrements('id');
            $table->unsignedInteger('form_id');
            $table->longText('data');
            $table->foreign('form_id')->references('id')->on('forms')->onUpdate('cascade')->onDelete('cascade');
            $table->string('ip',50);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('form_entries');
        Schema::dropIfExists('forms');
    }
}
