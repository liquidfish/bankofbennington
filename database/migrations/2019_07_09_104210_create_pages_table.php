<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePagesTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('pages',function(Blueprint $t)
        {
            $t->engine = 'InnoDB';
            $t->increments('id');
            $t->string('nav_title',75);
            $t->string('uri',768)->collation(config('larafish.uri_column_collation','utf8mb4_unicode_520_ci'))->unique();
            $t->unsignedInteger('depth')->default(1);
            $t->string('redirect_url')->nullable();
            $t->boolean('published')->default(false);
            $t->boolean('in_nav')->default(false);
            $t->integer('priority')->default(0);
            $t->string('required_permission_redirect_to')->nullable();
            $t->timestamps();
            $t->softDeletes();
        });
        Schema::create('page_versions',function(Blueprint $t)
        {
            $t->engine = 'InnoDB';
            $t->bigIncrements('id');
            $t->unsignedInteger('page_id');
            $t->foreign('page_id')->references('id')->on('pages')->onUpdate('cascade')->onDelete('cascade');
            $t->string('title',75);
            $t->boolean('active')->default(false);
            $t->text('meta_tags')->nullable();
            $t->text('extra_body_classes')->nullable();
            $t->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('page_versions');
        Schema::drop('pages');
    }

}