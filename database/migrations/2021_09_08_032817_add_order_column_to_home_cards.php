<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddOrderColumnToHomeCards extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('home_cards', function (Blueprint $table) {
            $table->integer('order')->default(0);
        });
        Schema::table('footer_links', function (Blueprint $table) {
            $table->integer('order')->default(0);
        });
        Schema::table('login_links', function (Blueprint $table) {
            $table->integer('order')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('home_cards', function (Blueprint $table) {
            $table->dropColumn('order');
        });
        Schema::table('footer_links', function (Blueprint $table) {
            $table->dropColumn('order');
        });
        Schema::table('login_links', function (Blueprint $table) {
            $table->dropColumn('order');
        });
    }
}
