<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('users', function(Blueprint $t)
		{
            $t->engine = 'InnoDB';
            $t->increments('id');
            $t->string('first_name', 50);
            $t->string('last_name', 50);
            $t->string('email', 255)->unique();
            $t->string('password', 64);
            $t->rememberToken();
            $t->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::drop('users');
	}

}