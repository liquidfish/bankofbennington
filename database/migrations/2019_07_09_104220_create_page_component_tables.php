<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePageComponentTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('page_components',function(Blueprint $table){
            $table->engine = 'InnoDB';
            $table->bigIncrements('id');
            $table->unsignedBigInteger('version_id');
            $table->foreign('version_id')->references('id')->on('page_versions')->onUpdate('cascade')->onDelete('cascade');
            $table->string('component');
            $table->unsignedInteger('order');
            $table->unsignedBigInteger('parent_component_id')->nullable();
            $table->foreign('parent_component_id')->references('id')->on('page_components')->onUpdate('cascade')->onDelete('set null');
            $table->string('bowl')->nullable();
            $table->longText('data');
            $table->longText('content')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('page_components');
    }
}
