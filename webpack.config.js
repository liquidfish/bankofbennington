const path = require('path');
const webpack = require('webpack');

module.exports = {
    resolve: {
        extensions: ['.js', '.vue', '.json'],
        alias: {
            '@': path.resolve('resources/js'),
        },
    },
};