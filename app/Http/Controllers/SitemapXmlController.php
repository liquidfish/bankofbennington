<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Liquidfish\Larafish\Page\Page;

class SitemapXmlController extends Controller
{

    public function generate() {
        $pages = Page::where('published', 1)->orderBy('uri', 'asc')->get();
        foreach($pages as $page) {
            $pagePriority = 0;
            switch($page->depth) {
                case 0:
                    $pagePriority = 1;
                    break;
                case 1:
                    $pagePriority = 0.8;
                    break;
                case 2:
                    $pagePriority = 0.75;
                    break;
                default:
                    $pagePriority = 0.5;
            }
            $page['sitemap_priority'] = $pagePriority;
        }
        return response()->view('sitemap.index', [
            'pages' => $pages
        ])->header('Content-Type', 'text/xml');
    }
}
