<?php

namespace App\Http\Controllers\Admin;

use App\Models\Announcement;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class AnnouncementsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $announcement = Announcement::first();
        return response()->view('admin.announcements.edit',['announcement' => $announcement]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Announcement $announcement)
    {
        $validator = Validator::make($request->all(), [
            'message' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->route('admin.announcements.index')->with('error', $validator->errors()->first());
        }

        $input = $request->all();
        $input['enabled'] = isset($input['enabled']) ? true : false;

        try {
            # New location creation
            $announcement->update($input);
            return redirect()->route('admin.announcements.index', $announcement->id)->with('success', 'Announcement Updated');
        } catch (\Exception $ex) {
            report($ex);
            return redirect()->route('admin.announcements.index')->with('error', $ex->getMessage());
        }
    }

}
