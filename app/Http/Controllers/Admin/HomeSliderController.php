<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\HomeSlider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class HomeSliderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $slides = HomeSlider::orderBy('order', 'asc')->get();
        return response()->view('admin.home-slider.list',['slides' => $slides]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return response()->view('admin.home-slider.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $validator = Validator::make($data, [
            'title' => 'required',
            'content' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->route('admin.home-slider.create')->with('error', $validator->errors()->first());
        }

        $data['active'] = isset($data['active']) ? true : false;
        try {
            $slide = HomeSlider::create($data);
            if(isset($data['image'])) $this->storeSlideImage($slide,$request);
            $slide->save();

            return redirect()->route('admin.home-slider.edit', $slide->id)->with('success', 'Slide Created');
        } catch (\Exception $ex) {
            report($ex);
            return redirect()->route('admin.home-slider.create')->with('error', $ex->getMessage());
        }
    }

    private function storeSlideImage(HomeSlider $slide,Request $request){
        if($request->hasFile('image')){
            $filename = $slide->id.'_'.
                str_slug($slide->title).
                '.'.$request->file('image')->getClientOriginalExtension();
            $slide->image = $this->fileUpload($request, $filename,'slides');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $slide = HomeSlider::find($id);
        return response()->view('admin.home-slider.edit', ['slide' => $slide]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        $validator = Validator::make($data, [
            'title' => 'required',
            'content' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->route('admin.home-slider.edit', $id)->with('error', $validator->errors()->first());
        }

        try {
            $data['active'] = isset($data['active']) ? true : false;
            $slide = HomeSlider::find($id);
            $slide->fill($data);
            if(isset($data['image'])) $this->storeSlideImage($slide,$request);
            $slide->save();

            return redirect()->route('admin.home-slider.edit', $slide->id)->with('success', 'Slide Updated');
        } catch (\Exception $ex) {
            report($ex);
            return redirect()->route('admin.home-slider.edit', $slide->id)->with('error', $ex->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        try {
            # Validation
            $this->validate($request, [
                'confirm_delete' => 'required|in:delete,Delete,DELETE'
            ]);
            $slide = HomeSlider::find($id);
            $slide->delete();
            return redirect()->route('admin.home-slider.index')->with('success', 'Slider Deleted');
        } catch (\Exception $ex) {
            return redirect()->route('admin.home-slider.index')->with('error', $ex->getMessage());
        }
    }

    /**
     * Stores file in storage
     * @param  \Illuminate\Http\Request  $request
     * @return string|null uploaded file path
     */
    public function fileUpload($request, $name, $directory) {
        $path = $request->file('image')->storeAs($directory, $name ,'public');
        return '/storage/' . $path;
    }

    /**
     * Updates the sorting order of the homepage slider
     * @param  \Illuminate\Http\Request  $request
     * @return object
     */
    public function sort(Request $request) {
        if($request->get('slides')) {
            $slides = $request->get('slides');
            foreach($slides as $index => $slide) {
                $slide = HomeSlider::find($slide['id']);
                $slide->order = $index;
                $slide->save();
            }
        }
        return response()->json(['success' => true]);
    }
}
