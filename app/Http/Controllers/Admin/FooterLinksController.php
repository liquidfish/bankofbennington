<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\FooterLink;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class FooterLinksController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $links = FooterLink::orderBy('order', 'asc')->get();
        return response()->view('admin.footer-links.list',['links' => $links]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return response()->view('admin.footer-links.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $validator = Validator::make($data, [
            'title' => 'required',
            'content' => 'required',
            'button_text' => 'required',
            'button_url' => 'required'
        ]);

        if ($validator->fails()) {
            return redirect()->route('admin.footer-links.create')->with('error', $validator->errors()->first());
        }

        $data['active'] = isset($data['active']) ? true : false;
        try {
            $link = FooterLink::create($data);
            if(isset($data['image'])) $this->storeFooterLinkImage($link,$request);
            $link->save();

            return redirect()->route('admin.footer-links.edit', $link->id)->with('success', 'Footer Link Created');
        } catch (\Exception $ex) {
            report($ex);
            return redirect()->route('admin.footer-links.create')->with('error', $ex->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $link = FooterLink::find($id);
        return response()->view('admin.footer-links.edit', ['link' => $link]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        $validator = Validator::make($data, [
            'title' => 'required',
            'content' => 'required',
            'button_text' => 'required',
            'button_url' => 'required'
        ]);

        if ($validator->fails()) {
            return redirect()->route('admin.footer-links.edit', $id)->with('error', $validator->errors()->first());
        }

        try {
            $data['active'] = isset($data['active']) ? true : false;
            $link = FooterLink::find($id);
            $link->fill($data);
            if(isset($data['image'])) $this->storeFooterLinkImage($link,$request);
            $link->save();

            return redirect()->route('admin.footer-links.edit', $link->id)->with('success', 'Footer Link Updated');
        } catch (\Exception $ex) {
            report($ex);
            return redirect()->route('admin.footer-links.edit', $link->id)->with('error', $ex->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request)
    {
        try {
            # Validation
            $this->validate($request, [
                'confirm_delete' => 'required|in:delete,Delete,DELETE'
            ]);
            $slide = FooterLink::find($id);
            $slide->delete();
            return redirect()->route('admin.footer-link.index')->with('success', 'Footer Link Deleted');
        } catch (\Exception $ex) {
            return redirect()->route('admin.footer-link.index')->with('error', $ex->getMessage());
        }
    }

    private function storeFooterLinkImage(FooterLink $link,Request $request){
        if($request->hasFile('image')){
            $filename = $link->id.'_'.
                str_slug($link->title).
                '.'.$request->file('image')->getClientOriginalExtension();
            $link->image = $this->fileUpload($request, $filename,'links');
        }
    }

    /**
     * Stores file in storage
     * @param  \Illuminate\Http\Request  $request
     * @return string|null uploaded file path
     */
    public function fileUpload($request, $name, $directory) {
        $path = $request->file('image')->storeAs($directory, $name ,'public');
        return '/storage/' . $path;
    }

    /**
     * Updates the sorting order of the homepage footer links
     * @param  \Illuminate\Http\Request  $request
     * @return object
     */
    public function sort(Request $request) {
        if($request->get('links')) {
            $links = $request->get('links');
            foreach($links as $index => $link) {
                $link = FooterLink::find($link['id']);
                $link->order = $index;
                $link->save();
            }
        }
        return response()->json(['success' => true]);
    }
}
