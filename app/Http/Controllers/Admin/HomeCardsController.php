<?php

namespace App\Http\Controllers\Admin;

use App\Models\HomeCard;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\ImageManagerStatic as Image;


class HomeCardsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cards = HomeCard::orderBy('order', 'asc')->get();
        return response()->view('admin.home-cards.list',['cards' => $cards]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return response()->view('admin.home-cards.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $validator = Validator::make($data, [
            'title' => 'required',
            'content' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->route('admin.home-cards.create')->with('error', $validator->errors()->first());
        }

        $data['active'] = isset($data['active']) ? true : false;
        try {
            $card = HomeCard::create($data);
            if(isset($data['image'])) $this->storeCardImage($card,$request);
            $card->save();

            return redirect()->route('admin.home-cards.edit', $card->id)->with('success', 'Home Card Created');
        } catch (\Exception $ex) {
            report($ex);
            return redirect()->route('admin.home-cards.create')->with('error', $ex->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $card = HomeCard::find($id);
        return response()->view('admin.home-cards.edit', ['card' => $card]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        $validator = Validator::make($data, [
            'title' => 'required',
            'content' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->route('admin.home-cards.edit', $id)->with('error', $validator->errors()->first());
        }

        $data['active'] = isset($data['active']) ? true : false;
        try {
            $card = HomeCard::find($id);
            $card->fill($data);
            if(isset($data['image'])) $this->storeCardImage($card,$request);
            $card->save();

            return redirect()->route('admin.home-cards.edit', $card->id)->with('success', 'Home Card Updated');
        } catch (\Exception $ex) {
            report($ex);
            return redirect()->route('admin.home-cards.edit', $card->id)->with('error', $ex->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        try {
            # Validation
            $this->validate($request, [
                'confirm_delete' => 'required|in:delete,Delete,DELETE'
            ]);
            $slide = HomeCard::find($id);
            $slide->delete();
            return redirect()->route('admin.home-cards.index')->with('success', 'Home Card Deleted');
        } catch (\Exception $ex) {
            return redirect()->route('admin.home-cards.index')->with('error', $ex->getMessage());
        }
    }

    private function storeCardImage(HomeCard $card, Request $request){
        if($request->hasFile('image')){
            $filename = $card->id.'_'.
                str_slug($card->title).
                '.'.$request->file('image')->getClientOriginalExtension();
            $card->image = $this->fileUpload($request, $filename,'app/public/cards/');
        }
    }

    /**
     * Stores file in storage
     * @param  \Illuminate\Http\Request  $request
     * @return string|null uploaded file path
     */
    public function fileUpload($request, $name, $directory) {
        $image = Image::make($request->file('image')->getRealPath());
        $image->fit(430,240)->save(storage_path($directory) . $name);
        return '/storage/cards/' . $name;
    }

    /**
     * Updates the sorting order of the homepage cards
     * @param  \Illuminate\Http\Request  $request
     * @return object
     */
    public function sort(Request $request) {
        if($request->get('cards')) {
            $cards = $request->get('cards');
            foreach($cards as $index => $card) {
                $homeCard = HomeCard::find($card['id']);
                $homeCard->order = $index;
                $homeCard->save();
            }
        }
        return response()->json(['success' => true]);
    }
}
