<?php

namespace App\Http\Controllers\Admin;

use App\Models\LoginLink;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class LoginLinksController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $links = LoginLink::orderBy('order', 'asc')->get();
        return response()->view('admin.login-links.list',['links' => $links]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return response()->view('admin.login-links.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $validator = Validator::make($data, [
            'button_text' => 'required',
            'button_url' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->route('admin.login-links.create')->with('error', $validator->errors()->first());
        }

        $data['active'] = isset($data['active']) ? true : false;
//        $data['is_sign_up'] = isset($data['is_sign_up']) ? true : false;
        try {
            $link = LoginLink::create($data);
            $link->save();

            return redirect()->route('admin.login-links.edit', $link->id)->with('success', 'Login Link Created');
        } catch (\Exception $ex) {
            report($ex);
            return redirect()->route('admin.login-links.create')->with('error', $ex->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $link = LoginLink::find($id);
        return response()->view('admin.login-links.edit', ['link' => $link]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        $validator = Validator::make($data, [
            'button_text' => 'required',
            'button_url' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->route('admin.login-links.edit')->with('error', $validator->errors()->first());
        }

        $data['active'] = isset($data['active']) ? true : false;
        $data['is_sign_up'] = isset($data['is_sign_up']) ? true : false;
        $this->updateSignUpButton($data, $id);
        try {
            $link = LoginLink::find($id);
            $link->fill($data);
            $link->save();

            return redirect()->route('admin.login-links.edit', $link->id)->with('success', 'Login Link Updated');
        } catch (\Exception $ex) {
            report($ex);
            return redirect()->route('admin.login-links.edit')->with('error', $ex->getMessage());
        }
    }

    protected function updateSignUpButton($formData, $id) {
        if(isset($formData['is_sign_up']) && $formData['is_sign_up']) {
            $link = LoginLink::where('id', '!=', $id)->where('is_sign_up', '=', 1)->first();
            if($link) {
                $link->is_sign_up = 0;
                $link->save();
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        try {
            # Validation
            $this->validate($request, [
                'confirm_delete' => 'required|in:delete,Delete,DELETE'
            ]);
            $slide = LoginLink::find($id);
            $slide->delete();
            return redirect()->route('admin.login-links.index')->with('success', 'Login Link Deleted');
        } catch (\Exception $ex) {
            report($ex);
            return redirect()->route('admin.login-links.index')->with('error', $ex->getMessage());
        }
    }

    /**
     * Updates the sorting order of the login links
     * @param  \Illuminate\Http\Request  $request
     * @return object
     */
    public function sort(Request $request) {
        if($request->get('links')) {
            $links = $request->get('links');
            foreach($links as $index => $link) {
                $loginLink = LoginLink::find($link['id']);
                $loginLink->order = $index;
                $loginLink->save();
            }
        }
        return response()->json(['success' => true]);
    }
}
