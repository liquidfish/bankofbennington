<?php

namespace App\Http\Composers;

use App\Models\Announcement;
use App\Models\FooterLink;
use Illuminate\View\View;

class FooterLinksComposer
{

    public function compose(View $view)
    {
        $links = FooterLink::where('active', true)->orderBy('order', 'asc')->get();
        $view->with(['links' => $links]);
    }

}
