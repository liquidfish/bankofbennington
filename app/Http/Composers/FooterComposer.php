<?php

namespace App\Http\Composers;

use App\Models\Announcement;
use App\Models\Footer;
use App\Models\FooterLink;
use Illuminate\View\View;

class FooterComposer
{

    public function compose(View $view)
    {
        $footer = Footer::first();
        $view->with(['footer' => $footer]);
    }

}
