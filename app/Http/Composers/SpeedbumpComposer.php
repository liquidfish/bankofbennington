<?php

namespace App\Http\Composers;

use App\Models\Announcement;
use App\Models\Speedbump;
use Illuminate\View\View;

class SpeedbumpComposer
{

    public function compose(View $view)
    {
        $whitelist = Speedbump::pluck('domain')->all();
        $view->with(['whitelist' => $whitelist]);
    }

}
