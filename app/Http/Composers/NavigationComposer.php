<?php

namespace App\Http\Composers;

use Illuminate\Support\Arr;
use Illuminate\View\View;
use \Liquidfish\Larafish\Menus\MenuRepository as MenuRepository;

class NavigationComposer
{
    protected $menus;
    public function __construct(MenuRepository $menus)
    {
        $this->menus = $menus;
    }


    public function compose(View $view)
    {
        $mainMenu = $this->menus->byUriAndDepth('/', 5);
        $view->with(['mainMenu' => $mainMenu]);
    }

}
