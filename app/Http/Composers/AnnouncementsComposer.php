<?php

namespace App\Http\Composers;

use App\Models\Announcement;
use Illuminate\View\View;

class AnnouncementsComposer
{

    public function compose(View $view)
    {
        $announcement = Announcement::first();
        $view->with(['announcement' => $announcement]);
    }

}
