<?php

namespace App\Http\Composers;

use App\Models\Announcement;
use App\Models\LoginLink;
use Illuminate\View\View;

class LoginLinksComposer
{

    public function compose(View $view)
    {
        $links = LoginLink::where('active','=',1)->orderBy('order', 'asc')->get();
        $view->with(['links' => $links]);
    }

}
