<?php

namespace App\Providers;
use App\Http\Composers\AnnouncementsComposer;
use App\Http\Composers\FooterComposer;
use App\Http\Composers\FooterLinksComposer;
use App\Http\Composers\LoginLinksComposer;
use App\Http\Composers\NavigationComposer;
use App\Http\Composers\SpeedbumpComposer;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class ComposerServiceProvider extends ServiceProvider
{
    public function register(){
        //
    }
    public function boot(){
        View::composer(['components.nav-menu'],NavigationComposer::class);
        View::composer(['components.announcements'],AnnouncementsComposer::class);
        View::composer(['components.speed-bump'],SpeedbumpComposer::class);
        View::composer(['components.login-modal'],LoginLinksComposer::class);
        View::composer(['components.links'],FooterLinksComposer::class);
        View::composer(['footer'],FooterComposer::class);
    }
}
