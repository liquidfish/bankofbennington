<?php namespace App\Components;

class Staff extends \Liquidfish\Larafish\Component\Component {
	protected $view = 'components.staff';
    protected $displayName = 'Staff';
    public $defaultData = ['people'=>[]];
}