<?php namespace App\Components;

use Illuminate\Support\Collection;
use Liquidfish\Larafish\Page\Component\PageComponent as BasePageComponent;

class Table extends \Liquidfish\Larafish\Component\Component {
    protected $view = 'components.table';
    protected $defaultData = [
        'inContainer'=>false,
        'tableClasses'=>['is-fullwidth is-striped'],
        'columns'=>[
            ['title'=>'Column1','classes'=>['text-white bg-bank-mid-green']],
            ['title'=>'Column2','classes'=>['text-white bg-bank-mid-green']]
        ],
        'rows'=>[
            [['contents'=>'<p>cell1</p>'],['contents'=>'<p>cell2</p>']],
            [['contents'=>'<p>cell3</p>'],['contents'=>'<p>cell4</p>']],
        ],
    ];

    public function prepareEditDataForSaving(BasePageComponent $pageComponent, $data){
        /** @var Collection $data */
        $content = data_get($data,'content');
        if(!empty($content)) {
            $pageComponent->content = preg_replace('/\n{2}\n+/',"\n\n",$content);
            unset($data['content']);
        } else {
            $pageComponent->content = null;
        }
        $pageComponent->data = $data;
    }
}