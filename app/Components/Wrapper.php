<?php namespace App\Components;

/**
 * Class DefaultLayout
 * The basic layout component
 */
class Wrapper extends RootComponent {
    protected $view = 'components.wrapper';
    protected $bowls = ['main'];
    protected $canBeLayout = true;
    protected $displayName = 'Spaced Wrapper';
}