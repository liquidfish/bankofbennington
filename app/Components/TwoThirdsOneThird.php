<?php namespace App\Components;

use Liquidfish\Larafish\Page\Component\PageComponent;

class TwoThirdsOneThird extends \Liquidfish\Larafish\Component\Component {
    public $displayName = '2/3 / 1/3 columns';
    public $view = 'components.two-thirds-one-third';
    public $bowls = ['main','sidebar'];
    protected $defaultSubComponents = [
        'main'=>['Larafish::ContentArea'],
        'sidebar'=>['Larafish::ContentArea']
    ];
    public $defaultData = [
        'bgColor'=>false,
    ];
    public function prepareDataForEdit(PageComponent $pageComponent)
    {
        return collect($this->defaultData)->merge($pageComponent->data);
    }
}