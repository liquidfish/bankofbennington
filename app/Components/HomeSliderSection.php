<?php namespace App\Components;

use App\Models\HomeSlider;
use Liquidfish\Larafish\Page\Component\PageComponent;

class HomeSliderSection extends \Liquidfish\Larafish\Component\Component {
    protected $view = 'pages.homepage.slider';
    protected $displayName = 'Homepage Slider Section';
    public $defaultData = [
        'heading' => '<h1>Your Money Stays Here, Works Here and that Makes a Difference.™</h1>'
    ];
    public function prepareDataForPage(PageComponent $pageComponent) {
        $data = $pageComponent->data;
        $data['slides'] = HomeSlider::where('active', true)->orderBy('order', 'asc')->get();
        return $data;
    }
}