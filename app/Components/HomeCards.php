<?php namespace App\Components;

use App\Models\HomeCard;
use Liquidfish\Larafish\Page\Component\PageComponent;

class HomeCards extends \Liquidfish\Larafish\Component\Component {
	protected $view = 'components.home-cards';
    protected $displayName = 'Homepage Cards';
    public $defaultData = [];
    public function prepareDataForPage(PageComponent $pageComponent) {
        $data = $pageComponent->data;
        $data['cards'] = HomeCard::where('active', true)->orderBy('order', 'asc')->get();
        return $data;
    }
}