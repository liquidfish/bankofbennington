<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LoginLink extends Model
{
    protected $fillable = ['button_url', 'button_text', 'active', 'is_sign_up'];
}
