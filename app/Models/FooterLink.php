<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FooterLink extends Model
{
    protected $fillable = ['title', 'content', 'button_text', 'button_url', 'active', 'image'];
    protected $appends = ['photo'];

    public function getPhotoAttribute() {
        return $this->image ? $this->image : '/img/svg/lock.svg';
    }
}
