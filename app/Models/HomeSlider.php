<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class HomeSlider extends Model
{
    protected $table = 'home_slides';
    protected $fillable = ['title','content','button_url','button_text','image','active', 'alt_text'];
    protected $appends = ['photo'];

    public function getPhotoAttribute() {
        return $this->image ? $this->image : '/img/slider.jpg';
    }
}
