<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class HomeCard extends Model
{
    protected $fillable = ['title', 'content', 'button_text', 'button_url', 'active', 'image', 'alt_text'];
    protected $appends = ['photo'];

    public function getPhotoAttribute() {
        return $this->image ? $this->image : '/img/img.jpg';
    }
}
