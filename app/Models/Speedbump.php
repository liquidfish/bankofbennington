<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Speedbump extends Model
{
    protected $table = 'speedbump';
    protected $fillable = ['domain'];
}
