<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Announcement extends Model
{
    protected $fillable = [
        'message',
        'enabled',
    ];

    protected $casts = [
        'enabled' => 'boolean',
    ];
}
