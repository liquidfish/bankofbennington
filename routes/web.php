<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\SitemapXmlController;

// Adding redirects for old links from previous website
Route::get('/management/', function() { return Redirect::to('/resources/management', 301); });
Route::get('/community/', function() { return Redirect::to('/resources/community', 301); });
Route::get('/savings/', function() { return Redirect::to('/personal-banking/savings', 301); });
Route::get('/commercial-lending/', function() { return Redirect::to('/business-banking/commercial-lending', 301); });
Route::get('/real-estate-construction', function() { return Redirect::to('/business-banking/commercial-lending', 301); });
Route::get('/acquisition-expansion/', function() { return Redirect::to('/business-banking/commercial-lending', 301); });
Route::get('/machinery-equipment/', function() { return Redirect::to('/business-banking/commercial-lending', 301); });
Route::get('/what-to-bring-with-you/', function() { return Redirect::to('/business-banking/commercial-lending', 301); });
Route::get('/commercial/', function() { return Redirect::to('/business-banking/commercial-lending', 301); });
Route::get('/sba-preferred-lender', function() { return Redirect::to('/business-banking/commercial-lending', 301); });
Route::get('/loc/', function() { return Redirect::to('/business-banking/commercial-lending', 301); });
Route::get('/business-accounts/', function() { return Redirect::to('/business-banking/business-services', 301); });
Route::get('/business-services/', function() { return Redirect::to('/business-banking/business-services', 301); });
Route::get('/wealth-management/', function() { return Redirect::to('financial-planning/investment-services', 301); });
Route::get('/mortgage/', function() { return Redirect::to('/lending/mortgages', 301); });
Route::get('/securlock', function() { return Redirect::to('/resources/securlock', 301); });
Route::get('/iras', function() { return Redirect::to('personal-banking/iras-invest-in-your-future', 301); });
Route::get('/checking', function() { return Redirect::to('personal-banking/checking', 301); });
Route::get('/locations', function() { return Redirect::to('contact-us', 301); });
Route::get('/careers', function() { return Redirect::to('resources/careers', 301); });
Route::get('/e-banking', function() { return Redirect::to('personal-banking/e-banking', 301); });
Route::get('/mobile-banking', function() { return Redirect::to('personal-banking/e-banking', 301); });
Route::get('/mortgage-faqs', function() { return Redirect::to('lending/mortgage-faqs', 301); });
Route::get('/deposit-rates', function() { return Redirect::to('personal-banking/deposit-rates', 301); });
Route::get('/consumer-loans', function() { return Redirect::to('lending/consumer-loan', 301); });
Route::get('/construction-loan', function() { return Redirect::to('lending/construction-loan', 301); });
Route::get('/remote-banking', function() { return Redirect::to('personal-banking/remote-banking', 301); });
Route::get('/financial-literacy', function() { return Redirect::to('resources/financial-literacy', 301); });
Route::get('/life-insurance', function() { return Redirect::to('financial-planning/life-insurance', 301); });
Route::get('/fdic-reg-privacy', function() { return Redirect::to('resources/fdicregsprivacy', 301); });
Route::get('/the-bank-of-benningtons-privacy-policy', function() { return Redirect::to('resources/fdicregsprivacy', 301); });
Route::get('/home-equity-loan', function() { return Redirect::to('lending/home-equity-loan', 301); });
Route::get('/how-to-switch', function() { return Redirect::to('resources/how-to-switch', 301); });
Route::get('/e-banking-best-practices', function() { return Redirect::to('resources/e-banking-best-practices', 301); });
Route::get('/keep-your-information-safe', function() { return Redirect::to('resources/e-banking-best-practices', 301); });
Route::get('/why-choose-the-bank-of-bennington', function() { return Redirect::to('resources/why-choose-the-bank-of-bennington', 301); });
Route::get('/about-us', function() { return Redirect::to('resources/why-choose-the-bank-of-bennington', 301); });
Route::get('/rates/deposit-rates', function() { return Redirect::to('personal-banking/deposit-rates', 301); });
Route::get('/people-pay', function() { return Redirect::to('personal-banking/remote-banking', 301); });
Route::get('/kids-savers/', function() { return Redirect::to('personal-banking/savings', 301); });

//Route::get('sitemap.xml', [SitemapXmlController::class, 'generate']);

Route::name('admin.')->prefix('admin')->namespace('Admin')->middleware('can:admin_area')->group(function() {
    Route::resource('announcements', 'AnnouncementsController');
    Route::resource('home-slider', 'HomeSliderController');
    Route::post('home-slider/sort', 'HomeSliderController@sort');

    Route::resource('login-links', 'LoginLinksController');
    Route::post('login-links/sort', 'LoginLinksController@sort');

    Route::resource('home-cards', 'HomeCardsController');
    Route::post('home-cards/sort', 'HomeCardsController@sort');

    Route::resource('footer-links', 'FooterLinksController');
    Route::post('footer-links/sort', 'FooterLinksController@sort');

    Route::resource('footer', 'FooterController');

    Route::resource('speedbump', 'SpeedBumpController');
});
