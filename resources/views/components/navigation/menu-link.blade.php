<input type="checkbox" id="{{ isset($index) ? 'drop-' . $index :  ''}}" class="hidden"/>
<label
    for="{{ isset($index) ? 'drop-' . $index :  ''}}"
    class="{{isset($class) ? $class : ''}} {{isset($active) && $active ? 'active' : ''}} hover:text-bank-yellow text-white font-bold menu-link">
    {{$slot}}
</label>