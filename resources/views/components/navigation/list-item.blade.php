<li class="md:inline-block {{isset($class) ? $class : ''}}">
    {{$slot}}
</li>