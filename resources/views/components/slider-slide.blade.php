{{--Desktop--}}
<div class="swiper-slide">
    <div class="md:p-10 px-4 py-5 bg-cover bg-center md:block hidden h-full"
         style="background-image: url({{$slide->photo}})">
        <div class="md:min-h-80 min-h-36 md:min-h-96 md:w-3/4 lg:w-1/2 flex flex-col gap-2 md:gap-10 items-start">
{{--            <p class="md:text-5xl text-2xl leading-4 md:leading-15 font-bold text-white slide-title">--}}
{{--                {{$slide->title}}--}}
{{--            </p>--}}
            <div class="slider-content">
                {!! $slide->content !!}
            </div>
            @if($slide->button_url && $slide->button_text)
                <a href="{{$slide->button_url}}" class="button">{{$slide->button_text}}</a>
            @endif
        </div>
    </div>
    <div class="md:hidden block h-full">
        <div class="slide-mobile">
            <img src="{{$slide->photo}}" alt="{{$slide->alt_text}}" class="mobile-slider-image">
        </div>
        <div class="md:min-h-80 min-h-36 h-full md:min-h-96 md:w-3/4 lg:w-1/2 flex flex-col gap-2 md:gap-10 items-start px-4 pt-4 pb-16 mobile-content-holder">
{{--            <p class="md:text-5xl text-2xl leading-4 md:leading-15 font-bold text-white slide-title relative">--}}
{{--                {{$slide->title}}--}}
{{--            </p>--}}
            <div class="slider-content relative">
                {!! $slide->content !!}
            </div>
            @if($slide->button_url && $slide->button_text)
                <a href="{{$slide->button_url}}" class="button">{{$slide->button_text}}</a>
            @endif
        </div>
    </div>
</div>
