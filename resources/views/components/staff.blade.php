<div id="component_{{$component->id}}"
     class="staff-component md:px-3 px-2 content"
     @can('update',$component)
     is="lf-staff"
     :component-id="{{$component->id}}"
     @endcan >
     @foreach($component->pageData->get('people') as $person)
          <div class="content-area flex mb-4">
               <div class="mb-1 mr-4 flex-shrink-0 profile-img">
                    @if(isset($person['imageUrl']) && $person['imageUrl'])
                         <img alt="{{isset($person['altText']) ? $person['altText'] : $person['name'] . ' Profile Image'}}" class="w-full" src="{{$person['imageUrl']}}">
                    @else
                         <div style="min-width: 100px; height: 140px; background: #efefef"></div>
                    @endif
               </div>

               <div class="mb-2 mr-4 profile-details flex-grow">
                    <h3>{{$person['name']}}</h3>
                    <b>{{$person['title']}}</b><br>
                    @if(!empty($person['department']))<b>{{$person['department']}}</b><br>@endif
                    @if(!empty($person['bio']))<p style="white-space: pre-wrap" class="profile-bio w-full">{{ $person['bio'] }}</p>@endif
{{--                    @if(!empty($person['location'])){{$person['location']}}<br>@endif--}}
                    @if(!empty($person['phone'])){{$person['phone']}}<br>@endif
                    @if(!empty($person['email']))<a class="text-bank-dark-green" href="mailto:{{$person['email']}}">{{$person['email']}}</a>@endif<br>
                    @if(!empty($person['long_bio']))
                        <lf-modal bio="{{ $person['long_bio'] }}"></lf-modal>
                   @endif
               </div>
          </div>
     @endforeach
</div>
