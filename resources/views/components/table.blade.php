<div id="component_{{$component->id}}" @can('update',$component) is="lfTable" :component-id="{{$component->id}}" @endcan >
    <div class="@if($component->pageData('inContainer',false)) table-container @endif overflow-x-scroll">
        <table class="table {{implode(' ',$component->pageData('tableClasses',[]))}}">
            <thead>
            <tr>
                @foreach($component->pageData('columns',[]) as $column)
                    <th class="{{implode(' ',$column['classes']??[])}}">{!! $column['title'] !!}</th>
                @endforeach
            </tr>
            </thead>
            <tfoot>
            </tfoot>
            <tbody>
            @foreach($component->pageData('rows',[]) as $row)
                <tr>
                    @foreach($row as $cell)
                        <td @if(isset($cell['colspan'])) colspan="{{$cell['colspan']}}" @endif class="content" v-pre>
                            {!! $cell['contents'] !!}
                        </td>
                    @endforeach
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>
