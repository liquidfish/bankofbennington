<div id="component_{{$component->id}}" class="content" @can('update',$component) is="lfYourVueControl" :component-id="{{$component->id}}" @endcan >
    {{-- Build out your component here. --}}
</div>