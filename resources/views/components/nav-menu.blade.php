<nav class="hidden md:block bg-bank-mid-green navigation-menu" aria-label="Main Navigation Menu">
    <div class="container-bank-xl mx-auto px-6 md:py-0 py-3">
        <ul class="menu relative w-full">
            @component('components.navigation.list-item')
                @component('components.navigation.menu-link',
                    ['class' => 'md:pl-6 md:py-5 py-2 pr-6 md:block flex'])
                    <a class="hover:text-bank-yellow {{ (request()->is('/')) ? 'active' : '' }}" href="{{$mainMenu->uri}}">{{$mainMenu->nav_title}}</a>
                @endcomponent
            @endcomponent
            @foreach($mainMenu->links ?? [] as $tierOneKey=>$tierOne)
                {{-- Tier One Menu Links --}}
                @if($tierOne->links)
                    @component('components.navigation.list-item')
                        @component('components.navigation.menu-link',
                            ['class' => 'md:px-6 md:py-5 py-2 flex justify-between md:block',
                             'index' => $tierOneKey])
                            {{$tierOne->nav_title}}
                            <span class="md:hidden toggle transition-all">@include('components.svg.arrow')</span>
                        @endcomponent

                        {{-- Tier Two Menu Links --}}
                        <ul class="z-20 hidden md:absolute">
                            @foreach($tierOne->links ?? [] as $tierTwoKey=>$tierTwo)
                                @component('components.navigation.list-item',
                                    ['class'=> ' md:inline-block m-0 text-white md:px-6 px-3 md:py-5 py-2 md:bg-bank-dark bg-bank-light-green w-full'])
                                    <a class="text-white hover:text-bank-yellow {{(request()->is($tierTwo->uri)) ? 'active' : ''}}" href="{{$tierTwo->path}}">{{$tierTwo->nav_title}}</a>
                                @endcomponent
                            @endforeach
                        </ul>
                    @endcomponent
                @else
                    @component('components.navigation.list-item')
                        @component('components.navigation.menu-link',
                            ['class' => 'md:px-6 md:py-5 py-2 md:block flex', 'index' => $tierOneKey])
                            <a class="hover:text-bank-yellow {{(request()->is($tierOne->uri)) ? 'active' : ''}}" href="/{{$tierOne->uri}}">{{$tierOne->nav_title}}</a>
                        @endcomponent
                    @endcomponent
                @endif
            @endforeach
        </ul>
    </div>
</nav>