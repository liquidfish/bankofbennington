<div class="card">
    <div class="py-3 px-5">
        <h2 class="card-title">{{$card->title}}</h2>
    </div>
    <img class="w-full" src="{{$card->photo}}" alt="{{$card->image_alt}}">
    <div class="card-content p-5">
        <p class="pb-6">{{$card->content}}</p>
        @if($card->button_text && $card->button_text)
            @component('components.generic.button', ['class' => 'transparent-btn small', 'href' => $card->button_url])
                {{$card->button_text}}
            @endcomponent
        @endif
    </div>
</div>