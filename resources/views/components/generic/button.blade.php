@php
    $defaultClasses = "font-bold font-titillium rounded tracking-wider";
@endphp
@if(isset($href))
    <a class="{{$defaultClasses}} {{isset($class) ? $class : ''}}" href="{{$href}}">{{$slot}}</a>
@else
    <button class="{{$defaultClasses}} {{isset($class) ? $class : ''}}" type="submit">{{$slot}}</button>
@endif
