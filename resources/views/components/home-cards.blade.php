@php
    $cards = $component->pageData('cards');
@endphp
<div id="component_{{$component->id}}"
     class="content"
     @can('update',$component)
     :component-id="{{$component->id}}"
     @endcan >
    <div class="grid grid-flow-row md:grid-cols-3 gap-4">
        @if(count($cards))
            @foreach($cards as $index => $card)
                @include('components.generic.card')
            @endforeach
        @endif
    </div>
</div>