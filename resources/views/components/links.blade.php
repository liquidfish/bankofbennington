<section class="container-bank-xl mx-auto flex md:flex-row flex-col items-start gap-4 md:py-16 py-10 md:px-10 px-4 links border-b-2 border-opacity-50 border-bank-base-mid">
    <div class="md:w-2/4 w-full md:pb-0 pb-8">
        <h2>{{config('content.footer-title')}}</h2>
    </div>
    <div class="grid grid-flow-row md:grid-cols-2 gap-8 flex-grow">
        @if(isset($links) && $links)
            @foreach($links as $link)
                <div class="flex items-start flex-row gap-8">
{{--                    <img src="/img/svg/lock.svg" alt="Lock">--}}
                    <div class="flex-grow">
                        <p class="font-bold text-xl leading-7 pb-3">{{$link->title}}</p>
                        <p class="font-normal leading-6 pb-7">{{$link->content}}</p>
                        @if($link->button_url && $link->button_text)
                            <a href="{{$link->button_url}}" class="tracking-wide font-bold text-bank-dark-green uppercase leading-4 pb-1 border-b-2 border-bank-dark-green">
                                {{$link->button_text}}
                            </a>
                        @endif
                    </div>
                </div>
            @endforeach
        @endif
    </div>
</section>