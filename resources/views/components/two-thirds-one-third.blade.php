<div id="component_{{$component->id}}"
     class="content has-top-border"
     @can('update',$component)
     is="lfTwoThirdsOneThird"
     :component-id="{{$component->id}}"
     @endcan >
    <div class="md:mx-auto md:flex md:flex-row-reverse">
        <div class="md:w-1/3">
            @bowl('sidebar')
        </div>
        <div class="md:w-2/3 md:mr-10">
            @bowl('main')
        </div>
    </div>
</div>