<header class="container-bank-xl mx-auto flex md:flex-row flex-col justify-between md:items-end items-start md:py-6 md:px-10 p-4">
    <a href="{{config('app.url')}}">@component('components.svg.logo', ['class' => 'md:w-auto w-full'])@endcomponent</a>
    <div class="flex justify-between items-end md:w-auto w-full md:mt-0 mt-4">
        @component('components.generic.button', ['class' => 'login-btn py-4 px-6 flex items-center uppercase bg-bank-yellow'])
            Login&nbsp;&nbsp; @include('components.svg.login-icon')
        @endcomponent
        <div class="md:hidden bg-bank-mid-green w-12 h-12 hamburger rounded relative">
            <input id="menu-toggle" type="checkbox" class="hidden" />
            <label
                class="menu-btn cursor-pointer w-6 h-6 flex absolute items-center
                top-1/2 left-1/2 transform -translate-y-2/4 -translate-x-2/4" for="menu-toggle">
                <span></span><span class="opacity-0">Hamburger</span>
            </label>
        </div>
    </div>
</header>