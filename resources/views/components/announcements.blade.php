@if($announcement && $announcement->enabled)
    <section class="bg-bank-yellow">
        <div class="container-bank-xl mx-auto md:px-12 px-4 py-3">
            <p class="font-bold text-bank-dark">{{$announcement ? $announcement->message : ''}}</p>
        </div>
    </section>
@endif