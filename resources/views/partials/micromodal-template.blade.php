@singleton('micromodal-template') @push('footer-scripts')
    <template id="micromodal-template">
        <div class="modal micromodal-slide" aria-hidden="true">
            <section class="modal-overlay" tabindex="-1" data-micromodal-close>
                <article class="modal-overlay-container-half" role="dialog" aria-modal="true" aria-labelledby="modal-title">
                    <div class="is-flex is-flex-column is-clipped is-fullwidth">
                        <header class="modal-overlay-container-header">
                            <h1 class="modal-overlay-container-header-title" id="modal-title"></h1>
                        </header>
                        <div class="modal-overlay-container-content" id="modal-content"></div>
                        <button class="close-modal btn" tabindex="0" aria-label="Close modal" data-micromodal-close></button>
                    </div>
                </article>
            </section>
        </div>
    </template>
@endpush @endsingleton