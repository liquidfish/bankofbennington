@php
    $slides = $component->pageData('slides');
@endphp
<section id="component_{{$component->id}}" class="content" @can('update',$component) :component-id="{{$component->id}}" @endcan>
    @if(count($slides))
        <div class="swiper-container slider">
            <div class="swiper-wrapper">
                @foreach($slides as $index => $slide)
                    @include('components.slider-slide')
                @endforeach
            </div>
            <div class="swiper-pagination"></div>
        </div>
    @else
        <section class="text-center p-4">
            <p class="font-bold text-2xl">No Slides Found!</p>
        </section>
    @endif
    <section class="md:p-10 py-5 px-4 bg-white text-center">
        @componentSlot(heading)
    </section>
</section>