@extends('larafish::layouts.master')

@section('title', $page->title)
@push('body_classes', implode(' ',$page->body_classes))
@include('larafish::layouts.head.meta',['meta_tags'=>$page->meta_tags])

@section('content')
    @can('update',$page)
        @include('larafish::layouts.page-editing')
    @endcan
    @includeFirst($page->componentTree->view,['component'=>$page->componentTree])
@endsection
