@extends('larafish::layouts.admin.master')

@section('title', 'Pages')

@section('content')
    <lf-pages-manager class="container box has-margin-top-size-3" :pages='@json($pages)'></lf-pages-manager>
@endsection
