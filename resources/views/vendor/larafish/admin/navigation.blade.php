@push('body_classes',' has-navbar-fixed-bottom')
<div id="larafish-main-nav" class="larafish">
    <lf-admin-navigation class="is-fixed-bottom">
        <template v-slot:brand>
            <a class="navbar-item" {{--href="https://liquid.fish"--}}>
                @include('larafish::admin.logo',['width'=>'56px','height'=>'28px'])
            </a>
        </template>
        <template v-slot:start>
            <b-navbar-dropdown is-up is-hoverable>
                <template v-slot:trigger>
                    <b-icon icon="account-group" size="is-small"></b-icon>
                    <span>Users</span>
                </template>

                <a href="/admin/users" class="navbar-item">
                    <b-icon icon="account-details" size="is-small"></b-icon>
                    <span>Manage</span>
                </a>
                <a href="/admin/roles" class="navbar-item">
                    <b-icon icon="account-key" size="is-small"></b-icon>
                    <span>Roles</span>
                </a>
            </b-navbar-dropdown>
            <a class="navbar-item" href="/admin/pages">
                <b-icon icon="book" size="is-small"></b-icon> <span>Pages</span>
            </a>
            <a class="navbar-item" href="/admin/files">
                <b-icon icon="file-multiple" size="is-small"></b-icon> <span>File Manager</span>
            </a>
{{--            <a class="navbar-item" href="/admin/forms">--}}
{{--                <b-icon icon="file-document-box" size="is-small"></b-icon> <span>Forms</span>--}}
{{--            </a>--}}
            {{ $slot ?? '' }}

            <b-navbar-dropdown is-up is-hoverable>
                <template v-slot:trigger>
                    <b-icon icon="bank" size="is-small"></b-icon>
                    <span>Bank Of Bennington Settings</span>
                </template>

                <a class="navbar-item" href="/admin/login-links">
                    <b-icon icon="comment-outline" size="is-small"></b-icon> <span> Account Login Links</span>
                </a>
                <a class="navbar-item" href="/admin/announcements">
                    <b-icon icon="sign-caution" size="is-small"></b-icon> <span>Announcements</span>
                </a>
                <a class="navbar-item" href="/admin/footer">
                    <b-icon icon="home-map-marker" size="is-small"></b-icon> <span> Footer</span>
                </a>
                <a class="navbar-item" href="/admin/home-cards">
                    <b-icon icon="home-map-marker" size="is-small"></b-icon> <span> Home Cards</span>
                </a>
                <a class="navbar-item" href="/admin/footer-links">
                    <b-icon icon="link" size="is-small"></b-icon> <span> Home Footer Links</span>
                </a>
                <a class="navbar-item" href="/admin/home-slider">
                    <b-icon icon="file-image" size="is-small"></b-icon> <span>Home Slider</span>
                </a>
                <a class="navbar-item" href="/admin/speedbump">
                    <b-icon icon="home" size="is-small"></b-icon> <span>Speed Bump Notification</span>
                </a>
            </b-navbar-dropdown>
        </template>
        <template v-slot:end>
            {{$endLeft??''}}
            <b-navbar-dropdown is-up is-hoverable>
                <template v-slot:trigger>
                    <b-icon icon="account" size="is-small"></b-icon>
                    <span>Hey {{ Larafish::user()->first_name }}</span>
                </template>
                <form class="navbar-item" action="{{URL::route('logout')}}" method="post">
                    <button type="submit" class="button is-warning">
                        <b-icon icon="logout" size="is-small"></b-icon>
                        <span>Logout</span>
                    </button>
                    @csrf
                </form>
            </b-navbar-dropdown>
            {{$end ?? ''}}
        </template>
    </lf-admin-navigation>
</div>
