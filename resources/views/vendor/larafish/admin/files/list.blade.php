@extends('larafish::layouts.admin.master')

@section('title', 'File Manager')

@section('content')
    <section class="container box has-margin-top-size-3">
        <header>
            <div class="level">
                <div class="level-left">
                    <div class="level-item">
                        <h1 class="is-size-1">File Manager</h1>
                    </div>
                </div>
            </div>
        </header>
        <lf-file-manager></lf-file-manager>
    </section>
@endsection
