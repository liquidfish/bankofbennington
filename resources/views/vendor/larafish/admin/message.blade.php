@if(session('message') || session('success') || session('error') || session('exception'))
	<section class="columns is-centered">
		<div class="column is-half">
			@if(session('message'))
				<b-message class="has-margin-top-size-6" title="" aria-close-label="Close message">
					{{session('message')}}
				</b-message>
			@endif
			@if(session('success'))
				<b-message class="has-margin-top-size-6" title="Success" type="is-success" title="" aria-close-label="Close message">
					{{session('success')}}
				</b-message>
			@endif
			@if(session('warning'))
				<b-message class="has-margin-top-size-6" title="" type="is-warning" title="" aria-close-label="Close message">
					{{session('warning')}}
				</b-message>
			@endif
			@if(session('error'))
				<b-message class="has-margin-top-size-6" title="" type="is-danger" title="" aria-close-label="Close message">
					{{session('error')}}
				</b-message>
			@endif
			@if(session('exception'))
				<b-message class="has-margin-top-size-6" title="Error" type="is-danger" title="" aria-close-label="Close message">
					{{session('exception')->getMessage()}}
				</b-message>
			@endif
		</div>
	</section>
@endif