@extends('larafish::layouts.admin.master')

@section('title', 'Users')

@section('content')
    <section class="container box has-margin-top-size-3">
        <header class="level">
            <div class="level-left">
                <div class="level-item">
                    <h1 class="is-size-1">Users</h1>
                </div>
            </div>
            <div class="level-right">
                <div class="level-item">
                    <a href="{{ route('admin.users.create') }}" title="Create User" class="button is-primary"><b-icon icon="account-plus"></b-icon> <span>Create User</span></a>
                </div>
            </div>
        </header>
        <b-table
            striped hoverable
            class="is-fullwidth"
            :data='@json($users)'
            paginated :per-page="20"
            default-sort="first_name"
            default-sort-direction="asc">
            <template v-slot="props">
                <b-table-column sortable field="first_name" label="First Name">@{{props.row.first_name}}</b-table-column>
                <b-table-column sortable field="last_name" label="Last Name">@{{props.row.last_name}}</b-table-column>
                <b-table-column sortable field="email" label="Email">@{{props.row.email}}</b-table-column>
                <b-table-column label="Roles">
                    <span class="tags">
                        <span class="tag" v-for="role in props.row.roles">@{{role.name}}</span>
                    </span>
                </b-table-column>
                <b-table-column label="Manage">
                    <a :href="props.row.canEdit ? '/admin/users/'+props.row.id+'/edit':''" :disabled="!props.row.canEdit" class="button is-link is-small"><b-icon icon="account-edit"></b-icon> <span>Edit</span></a>
                </b-table-column>
                @if(Larafish::user()->isSuperAdmin())
                    <b-table-column label="Switch">
                        <form method="post" :action="'/admin/users/'+props.row.id+'/switch'">
                            @csrf
                            <button class="button is-link is-small">
                                <b-icon icon="account-switch"></b-icon>
                                <span>Switch To</span></a>
                            </button>
                        </form>
                    </b-table-column>
                @endif
            </template>
        </b-table>
    </section>
@endsection

