@extends('larafish::layouts.admin.master')

@section('title', 'Role Editing - '.$role->name)

@section('content')
	<section class="container box has-margin-top-size-3">
		<header class="level">
			<div class="level-left">
				<div class="level-item">
					<h1 class="is-size-1">Editing {{$role->name}}</h1>
				</div>
			</div>
			<div class="level-right">
				<div class="level-item">
					<a href="{{ route('admin.roles.index') }}" title="View All Roles" class="button is-primary"><b-icon icon="account-key"></b-icon> <span>View Roles</span></a>
				</div>
			</div>
		</header>
		<div class="columns">
			<div class="column">
				<form method="post" action="{{ URL::route('admin.roles.update',$role->id) }}" class="form">
					<div class="field">
						<label class="label" for="name">Role Name</label>
						<p class="control">
							<input class="input" type="text" id="name" name="name" placeholder="Role Name" required value="{{ $role->name }}">
						</p>
					</div>
					<div class="field">
						<label class="label">Permissions</label>
						<div class="tags">
							@foreach($permissions as $permission)
								<label class="checkbox tag">
									<input type="checkbox" id="permission_{{ $permission->id }}" name="permissions[]" value="{{ $permission->id }}" @if($role->permissions->contains($permission->id)) checked @endif>&nbsp;
									<span>{{ $permission->display_name }}</span>
								</label>
							@endforeach
						</div>
					</div>
					<div class="control">
						<button type="submit" class="button is-primary">Save Role</button>
					</div>

					{{ method_field('PUT') }}
					{{ csrf_field() }}
				</form>
			</div>

			@can('delete',$role)
			<div class="column is-one-third">
				<form method="post" action="{{URL::route('admin.roles.destroy',$role->id)}}" class="form box is-danger">
					<div class="field">
						<label class="label has-text-danger" for="confirm_delete">Delete Role</label>
						<p class="control">
							<input class="input" type="text" id="confirm_delete" name="confirm_delete" placeholder="DELETE" required>
						</p>
						<p class="help has-text-danger">Type "DELETE" to confirm deletion.</p>
					</div>
					<div class="control">
						<button type="submit" class="button is-danger">Delete Role</button>
					</div>
					@method('DELETE')
					@csrf
				</form>
			</div>
			@endcan
		</div>

	</section>

@endsection