@extends('larafish::layouts.admin.master')

@section('title', 'Roles')

@section('content')
	<section class="container box has-margin-top-size-3">
		<header class="level">
			<div class="level-left">
				<div class="level-item">
					<h1 class="is-size-1">Roles</h1>
				</div>
			</div>
			<div class="level-right">
				<div class="level-item">
					<a href="{{ route('admin.roles.create') }}" title="Create Role" class="button is-primary"><b-icon icon="key-plus"></b-icon> <span>Create Role</span></a>
				</div>
			</div>
		</header>

		<b-table
				striped hoverable
				class="is-fullwidth"
				:data='@json($roles)'
				paginated :per-page="20"
				default-sort="name"
				default-sort-direction="asc">
			<template v-slot="props">
				<b-table-column sortable field="name" label="Display Name">@{{props.row.name}}</b-table-column>
				<b-table-column label="Permissions">
					<span class="tags"><span class="tag" v-for="permission in props.row.permissions">@{{permission.display_name}}</span></span>
				</b-table-column>
				<b-table-column label="Manage">
					<a :href="props.row.canEdit ? '/admin/roles/'+props.row.id+'/edit':''" :disabled="!props.row.canEdit" class="button is-link is-small"><b-icon icon="account-edit"></b-icon> <span>Edit</span></a>
				</b-table-column>
			</template>
		</b-table>
	</section>

@endsection