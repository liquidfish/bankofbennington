@extends('larafish::layouts.admin.master')

@section('title', 'Role Creation')

@section('content')
	<section class="container box has-margin-top-size-3">
		<header class="level">
			<div class="level-left">
				<div class="level-item">
					<h1 class="is-size-1">Role Creation</h1>
				</div>
			</div>
			<div class="level-right">
				<div class="level-item">
					<a href="{{ route('admin.roles.index') }}" title="View All Roles" class="button is-primary"><b-icon icon="account-key"></b-icon> <span>View Roles</span></a>
				</div>
			</div>
		</header>

		<form method="post" action="{{ URL::route('admin.roles.store') }}" class="form">
			<div class="field">
				<label class="label" for="name">Role Name</label>
				<p class="control">
					<input class="input" type="text" id="name" name="name" placeholder="Role Name" required>
				</p>
			</div>
			<div class="field">
				<label class="label">Permissions</label>
				<div class="tags">
					@foreach($permissions as $permission)
						<label class="checkbox tag">
							<input type="checkbox" id="permission_{{ $permission->id }}" name="permissions[]" value="{{ $permission->id }}">&nbsp;
							<span>{{ $permission->display_name }}</span>
						</label>
					@endforeach
				</div>
			</div>
			<div class="control">
				<button type="submit" class="button is-primary">Create Role</button>
			</div>

			{{ csrf_field() }}
		</form>
	</section>

@endsection