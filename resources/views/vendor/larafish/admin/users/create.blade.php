@extends('larafish::layouts.admin.master')

@section('title', 'User Creation')

@section('content')
	<section class="container box has-margin-top-size-3">
		<header class="level">
			<div class="level-left">
				<div class="level-item">
					<h1 class="is-size-1">User Creation</h1>
				</div>
			</div>
			<div class="level-right">
				<div class="level-item">
					<a href="{{ URL::route('admin.users.index') }}" title="View All Users" class="button is-primary"><b-icon icon="account-multiple"></b-icon><span>View All Users</span></a>
				</div>
			</div>
		</header>
		<form method="post" action="{{ URL::route('admin.users.store') }}" class="form">
			<div class="columns">
				<div class="column">
					<div class="field">
						<label class="label" for="first_name">First Name</label>
						<p class="control">
							<input class="input" type="text" id="first_name" name="first_name" placeholder="First Name" required>
						</p>
					</div>
				</div>
				<div class="column">
					<div class="field">
						<label class="label" for="last_name">Last Name</label>
						<p class="control">
							<input class="input" type="text" id="last_name" name="last_name" placeholder="Last Name" required>
						</p>
					</div>
				</div>
			</div>
			<div class="field">
				<label class="label" for="email">Email</label>
				<p class="control has-icons-left">
					<input class="input" type="email" id="email" name="email" placeholder="email@example.com" required>
					<b-icon icon="email" size="is-small" class="is-left"></b-icon>
				</p>
			</div>
			<b-field label="Password">
				<b-input type="password" id="password" name="password" placeholder="Choose a password" required
						 password-reveal icon="lock" minlength="6" validation-message="Please use more than 5 characters in your passwords.">
				</b-input>
			</b-field>

			<div class="field">
				<label class="label">Roles</label>
				<div class="tags">
					@foreach($roles as $role)
						<label class="checkbox tag">
							<input type="checkbox" id="role_{{ $role->id }}" name="roles[]" value="{{ $role->id }}">&nbsp;
							<span>{{ $role->name }}</span>
						</label>
					@endforeach
				</div>
			</div>

			<button type="submit" class="button is-primary">Save User</button>

			{{ csrf_field() }}
		</form>
	</section>
@endsection