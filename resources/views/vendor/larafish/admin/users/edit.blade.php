@extends('larafish::layouts.admin.master')

@section('title', 'User Editing - '.$user->fullName)

@section('content')
	<section class="container box has-margin-top-size-3">
		<header class="level">
			<div class="level-left">
				<div class="level-item">
					<h1 class="is-size-1">Editing {{ $user->fullName }}</h1>
				</div>
			</div>
			<div class="level-right">
				<div class="level-item">
					<a href="{{ URL::route('admin.users.index') }}" title="View All Users" class="button is-primary"><b-icon icon="account-multiple"></b-icon><span>View All Users</span></a>
				</div>
			</div>
		</header>
		<div class="columns">
			<div class="column">
				<form method="post" action="{{ URL::route('admin.users.update', $user->id) }}" class="form">
					<div class="columns">
						<div class="column">
							<div class="field">
								<label class="label" for="first_name">First Name</label>
								<p class="control">
									<input class="input" type="text" id="first_name" name="first_name" placeholder="First Name" value="{{ $user->first_name }}" required>
								</p>
							</div>
						</div>
						<div class="column">
							<div class="field">
								<label class="label" for="last_name">Last Name</label>
								<p class="control">
									<input class="input" type="text" id="last_name" name="last_name" placeholder="Last Name" value="{{ $user->last_name }}" required>
								</p>
							</div>
						</div>
					</div>
					<div class="field">
						<label class="label" for="email">Email</label>
						<p class="control has-icons-left">
							<input class="input" type="email" id="email" name="email" placeholder="email@example.com" value="{{ $user->email }}" required>
							<b-icon icon="email" size="is-small" class="is-left"></b-icon>
						</p>
					</div>
					@unless($user->isSuperAdmin())
						<b-field label="Password">
							<b-input type="password" id="password" name="password" placeholder="New password" password-reveal icon="lock"></b-input>
						</b-field>
					@endunless

					<div class="field">
						<label class="label">Roles</label>
						<div class="tags">
							@if($user->isSuperAdmin())
								<span class="tag">SuperAdmin</span>
							@else
								@foreach($roles as $role)
									<label class="checkbox tag">
										<input type="checkbox" id="role_{{ $role->id }}" name="roles[]" value="{{ $role->id }}" @if($user->roles->contains($role->id)) checked @endif>&nbsp;
										<span>{{ $role->name }}</span>
									</label>
								@endforeach
							@endif
						</div>
					</div>

					<button type="submit" class="button is-primary has-margin-top-size-6">Save User</button>

					{{ csrf_field() }}
					{{ method_field('put') }}
				</form>
			</div>

			@can('delete',$user)
				<div class="column is-one-third">
					<form method="post" action="{{URL::route('admin.users.destroy',$user->id)}}" class="form box is-danger">
						<div class="field">
							<label class="label has-text-danger" for="confirm_delete">Delete User</label>
							<p class="control">
								<input class="input" type="text" id="confirm_delete" name="confirm_delete" placeholder="DELETE" required>
							</p>
							<p class="help has-text-danger">Type "DELETE" to confirm deletion.</p>
						</div>
						<div class="control">
							<button type="submit" class="button is-danger">Delete User</button>
						</div>
						@method('DELETE')
						@csrf
					</form>
				</div>
			@endcan
		</div>
	</section>
@endsection