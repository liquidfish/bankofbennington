<div class="form-builder-success content">
    <h1 class="is-size-1">{{$success_title}}</h1>
    @if(!empty($success_description))
    <p>{!! $success_description !!}</p>
    @endif
</div>

