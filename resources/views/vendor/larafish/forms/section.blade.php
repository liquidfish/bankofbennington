<section class="form-builder-section">
    @if(!empty($section['title']))
        <h2 class="is-size-2">{{$section['title']}}</h2>
    @endif
    @foreach($section['fields'] as $field)
            {{--checkboxes, dropdown, file, photo, text, textarea, etc--}}
            @include('larafish::forms.fields.'.$field['type'],['field'=>$field])
    @endforeach
</section>