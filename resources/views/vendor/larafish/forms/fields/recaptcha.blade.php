<div class="field lf-form-recaptcha">
    <div id="{{$form->id}}-recaptcha" class="g-recaptcha"></div>
    @error('g-recaptcha-response')
    <p class="help is-danger">
        {{$message}}
    </p>
    @enderror
</div>
@singleton('form-assets-recaptcha')
@push('scripts')
    <script src="https://www.google.com/recaptcha/api.js?render=explicit" async defer></script>
    <script>
        window.addEventListener('load',function(){
            Array.from(document.querySelectorAll('.g-recaptcha')).forEach(function(recaptcha){
                grecaptcha.render(recaptcha,{sitekey:'{{config('larafish.recaptcha.key')}}'});
            });
        });
    </script>
@endpush
@endsingleton
