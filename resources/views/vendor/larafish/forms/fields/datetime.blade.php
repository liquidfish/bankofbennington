@extends('larafish::forms.fields.single-input-field')
@section('field-'.$field['name'].'-control')
    <div class="control lf-forms-@yield('dateTimeType-'.$field['name'])-field is-relative input-group">
        <input type="text" class="input" name="{{$field['name']}}" data-input
               id="{{$field['name']}}" @if($field['required']) required @endif value="{{old($field['name'])}}"
               @if($form->has_conditions || Gate::allows('admin_area')) is="lf-forms-@yield('dateTimeType-'.$field['name'])-input-value" data-field='@json($field)'
               @if(!empty($oldVal = old($field['name']))) data-old-value="{{$oldVal}}" @endif @endif>
        <button type="button" class="delete is-right-of-input" data-clear></button>
    </div>
@endsection
@cannot('admin_area')
    @singleton('form-assets-datetime')
        @if(!$form->has_conditions)
            @push('footer-scripts')
                <script defer type="text/javascript" src="{{ mix('js/forms-datetime.js') }}"></script>
            @endpush
        @endif
        @push('styles')
            <link rel="stylesheet" href="{{ mix('css/forms-datetime.css') }}">
        @endpush
    @endsingleton
@endcannot