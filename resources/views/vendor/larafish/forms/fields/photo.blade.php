@extends('larafish::forms.fields.single-input')
@section('field-'.$field['name'])
    <input type="file" name="{{$field['name']}}" class="input lf-forms-photo-field" id="{{$field['name']}}"
           @if($field['required']) required @endif accept="image/*"
           @if($field['multiple']) multiple @endif
           @if($form->has_conditions) is="lf-forms-input-value" data-field='@json($field)' @endif>
@endsection