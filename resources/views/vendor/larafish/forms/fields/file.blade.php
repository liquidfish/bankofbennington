@extends('larafish::forms.fields.single-input')
@section('field-'.$field['name'])
    <input type="file" name="{{$field['name']}}" class="input lf-forms-file-field" id="{{$field['name']}}"
           @if($field['required']) required @endif
           @if($field['multiple']) multiple @endif
           accept="{{implode(',',array_map(function($ext){return ".$ext";},$form::AllowedFileFormats))}}"
           @if($form->has_conditions) is="lf-forms-input-value" data-field='@json($field)' @endif>
@endsection