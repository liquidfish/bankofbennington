@extends('larafish::forms.fields.single-input')
@section('field-'.$field['name'])
    <input type="{{in_array($field['validator'],['url','email'])?$field['validator']:'text'}}" class="lf-input input"
           name="{{$field['name']}}" id="{{$field['name']}}"
           @if($field['required']) required @endif value="{{old($field['name'])}}"
           @if($form->has_conditions) is="lf-forms-input-value" data-field='@json($field)'
           @if(!empty($oldVal = old($field['name']))) data-old-value="{{$oldVal}}" @endif @endif>
@endsection