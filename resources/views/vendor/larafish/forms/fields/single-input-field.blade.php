<fieldset class="field" @if($field['conditional']) is="lf-forms-conditional-field" data-conditions='@json($field['conditions'])' @endif>
    <label class="label @if($field['required']) is-required @endif" for="{{$field['name']}}">
        {{$field['label']}}
    </label>
    @yield('field-'.$field['name'].'-control')
    @error($field['name'])
    <p class="help is-danger">
        {{$message}}
    </p>
    @enderror
</fieldset>
