@extends('larafish::forms.fields.single-input-field')
@section('field-'.$field['name'].'-control')
<div class="control">
    @yield('field-'.$field['name'])
</div>
@endsection
