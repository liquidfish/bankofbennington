<fieldset class="field">
    @if(count($field['options']) > 1)
        <legend class="label @if($field['required']) is-required @endif">
            {{$field['label']}}
        </legend>
    @elseif(count($field['options']) === 1)
        <label class="label @if($field['required']) is-required @endif" for="{{$field['name'].$field['options'][0]['id']}}-@yield('fieldtype-'.$field['name'])">
            {{$field['label']}}
        </label>
    @endif
    <div class="field is-grouped is-grouped-multiline" @if($form->has_conditions) is="lf-forms-group-input-value" data-field='@json($field)'
        @if(!empty($oldVal = old($field['name']))) data-old-value='@json($oldVal)' @endif @endif>
        @foreach($field['options'] as $option)
            <div class="control">
                <label class="@yield('fieldtype-'.$field['name'])">
                    <input type="@yield('fieldtype-'.$field['name'])" name="{{$field['name'].($field['type']==='checkboxes'?'[]':'')}}" value="{{$option['id']}}"
                           @if($field['required']) required @endif
                           @if(!empty($oldVal = old($field['name'])) &&
                           ($field['type']==='checkboxes'?in_array($option['id'],$oldVal):$oldVal===$option['id'])
                           ) checked @endif>
                    <span>{{$option['name']}}</span>
                </label>
            </div>
        @endforeach
    </div>
    @error($field['name'])
        <p class="help is-danger">
            {{$message}}
        </p>
    @enderror
</fieldset>
