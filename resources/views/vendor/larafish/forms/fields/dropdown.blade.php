@extends('larafish::forms.fields.single-input')
@section('field-'.$field['name'])
    <div class="select">
        <select name="{{$field['name']}}" id="{{$field['name']}}" @if($field['required']) required @endif
            @if($form->has_conditions) is="lf-forms-select-value" data-field='@json($field)'
            @if(!empty($oldVal = old($field['name']))) data-old-value="{{$oldVal}}" @endif @endif>
            <option value="">-</option>
            @foreach($field['options'] as $option)
                <option value="{{$option['id']}}" @if(old($field['name'])===$option['id']) selected @endif>{{$option['name']}}</option>
            @endforeach
        </select>
    </div>
@endsection