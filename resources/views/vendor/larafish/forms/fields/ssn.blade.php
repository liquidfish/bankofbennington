@extends('larafish::forms.fields.single-input')
@section('field-'.$field['name'])
    <input type="text" class="input" name="{{$field['name']}}" id="{{$field['name']}}" @if($field['required']) required @endif
        pattern="^(?!219-?09-?9999|078-?05-?1120)(?!666|000|9\d{2})\d{3}-?(?!00)\d{2}-?(?!0{4})\d{4}$"
        title="Expected pattern: ###-##-#### or #########" value="{{old($field['name'])}}"
           @if($form->has_conditions) is="lf-forms-input-value" data-field='@json($field)'
           @if(!empty($oldVal = old($field['name']))) data-old-value="{{$oldVal}}" @endif @endif>
@endsection