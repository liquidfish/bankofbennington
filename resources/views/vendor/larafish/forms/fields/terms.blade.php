<div class="field">
    <label class="label">Terms & Conditions</label>
    <div class="lf-form-terms-conditions">
        {!! nl2br(e($form->terms_conditions)) !!}
    </div>
    <p class="control terms-agree">
        <label class="checkbox">
            <input type="checkbox" required>
            <span>{{$form->terms_agree}}</span>
        </label>
    </p>
</div>