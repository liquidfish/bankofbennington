@extends('larafish::forms.fields.single-input')
@section('field-'.$field['name'])
    <textarea name="{{$field['name']}}" id="{{$field['name']}}" class="textarea lf-form-textarea"
              @if($field['required']) required @endif
              @php($oldVal = old($field['name']))
              @if($form->has_conditions) is="lf-forms-textarea-value" data-field='@json($field)'
              @if(!empty($oldVal)) data-old-value="{{$oldVal}}" @endif @endif
    >{{$oldVal}}</textarea>
@endsection