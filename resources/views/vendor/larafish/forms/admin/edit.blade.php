@extends('larafish::layouts.admin.master')

@section('title', 'Edit Form')
@section('content')
    <section class="container box has-margin-top-size-3">
        <header class="level">
            <div class="level-left">
                <div class="level-item">
                    <h1 class="is-size-1">Edit Form</h1>
                </div>
            </div>
            <div class="level-right">
                <div class="level-item">
                    <div class="buttons">
                        <a href="{{ route('admin.forms.index') }}" class="button is-primary"><b-icon icon="file-document-box-multiple"></b-icon> <span>View All Forms</span></a>
                        <lf-form-builder-edit-preview-button class="is-primary" preview-route="{{route('admin.forms.show',$form->id)}}"></lf-form-builder-edit-preview-button>
                    </div>
                </div>
            </div>
        </header>
        <lf-form-builder :form-id="{{$form->id}}" form-data-url="{{route('admin.forms.json',$form->id)}}"
                         form-save-url="{{route('admin.forms.update',$form->id)}}"></lf-form-builder>

        <form action="{{route('admin.forms.destroy',$form->id)}}" method="post" class="box is-danger">
            @method('delete')
            @csrf

            <div class="field">
                <label class="label has-text-danger">
                    DangerZone
                </label>
                <p class="help is-danger">
                    This will remove the Form, related entries and attached files. This is non-recoverable.
                </p>
                <input class="input" type="text" name="confirmation" placeholder="Type 'Delete' to confirm deletion" title="Type 'Delete' to confirm" pattern="^[Dd][Ee][Ll][Ee][Tt][Ee]$">
            </div>
            <div class="field">
                <button class="button is-danger">Delete</button>
            </div>
        </form>
    </section>
@endsection
