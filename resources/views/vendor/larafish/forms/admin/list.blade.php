@extends('larafish::layouts.admin.master')

@section('title', 'Forms')

@section('content')
    <section class="container box has-margin-top-size-3">
        <header class="level">
            <div class="level-left">
                <div class="level-item">
                    <h1 class="is-size-1">Forms</h1>
                </div>
            </div>
            <div class="level-right">
                <div class="level-item">
                    <a href="{{ route('admin.forms.create') }}" class="button is-primary"><b-icon icon="file-document-box-plus"></b-icon> <span>Create Form</span></a>
                </div>
            </div>
        </header>

        <b-table
                striped hoverable
                class="is-fullwidth"
                :data='@json($forms)'
                paginated :per-page="20"
                default-sort="title"
                default-sort-direction="asc">
            <template v-slot="props">
                <b-table-column sortable field="title" label="Title">@{{props.row.title}}</b-table-column>
                <b-table-column sortable field="entries_count" label="Entries">@{{props.row.entries_count}}</b-table-column>
                <b-table-column label="Manage">
                    <div class="buttons">
                        <a :href="'/admin/forms/'+props.row.id+'/edit'" class="button is-primary">
                            <b-icon icon="pencil"></b-icon>
                            <span>Edit</span>
                        </a>
                        <a :href="'/admin/forms/'+props.row.id" class="button is-primary">
                            <b-icon icon="file-document-box-search"></b-icon>
                            <span>Preview</span>
                        </a>
                        <b-dropdown aria-role="list">
                            <button class="button is-primary" slot="trigger">
                                <b-icon icon="download"></b-icon>
                                <span>Download Entries</span>
                                <b-icon icon="menu-down"></b-icon>
                            </button>
                            <b-dropdown-item aria-role="listitem"><a :href="'/admin/forms/'+props.row.id+'/entries/csv'">As CSV</a></b-dropdown-item>
                            <b-dropdown-item aria-role="listitem"><a :href="'/admin/forms/'+props.row.id+'/entries/excel'">For Excel</a></b-dropdown-item>
                        </b-dropdown>
                    </div>
                </b-table-column>
            </template>
            <template slot="empty">
                <h3 class="heading is-size-3">No forms have been created yet, please create one!</h3>
            </template>
        </b-table>
    </section>
@endsection
