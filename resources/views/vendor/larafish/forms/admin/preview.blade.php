@extends('larafish::layouts.admin.master')

@section('title','Form Preview: '.$form->title)

@section('content')
    <div class="container box has-margin-top-size-3">
        <h2 class="heading is-size-2">
            Form Preview
        </h2>
        @include('larafish::forms.form',$form)
    </div>
@endsection
