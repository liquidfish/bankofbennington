@extends('larafish::layouts.admin.master')

@section('title', 'Create Form')
@section('content')
<section class="container box has-margin-top-size-3">
    <header class="level">
        <div class="level-left">
            <div class="level-item">
                <h1 class="is-size-1">Create Form</h1>
            </div>
        </div>
        <div class="level-right">
            <div class="level-item">
                <a href="{{ route('admin.forms.index') }}" class="button is-primary"><b-icon icon="file-document-box-multiple"></b-icon> <span>View All Forms</span></a>
            </div>
        </div>
    </header>
    <form action="{{route('admin.forms.store')}}" method="post">

        @csrf

        <div class="field">
            <label for="form_title" class="label">Title</label>
            <div class="control">
                <input type="text" class="input" name="title" id="form_title" placeholder="Form Title" maxlength="255" required>
            </div>
        </div>
        <div class="field">
            <div class="control">
                <button class="button is-primary">Create Form...</button>
            </div>
        </div>

    </form>
</section>
@endsection