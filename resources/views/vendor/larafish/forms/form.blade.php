@can('submit',$form)
<form action="{{$form->endpoint}}" id="form_{{$form->id}}" method="post" class="lf-form"
      @if($form->has_files) enctype="multipart/form-data" @endif @if($form->has_conditions) data-conditional @endif >
    @csrf

{{--    <input type="hidden" name="form_id" value="{{$form->id}}">--}}
    @if(isset($page))<input type="hidden" name="page_id" value="{{$page->id}}">@endif

    @if(!empty($form->title))
        <h1 class="is-size-1">{{$form->title}}</h1>
    @endif

    @if(!empty($form->description))
        <div class="lf-form-description"><p>{!! nl2br(e($form->description)) !!}</p></div>
    @endif
    @error('form_'.$form->id)
    <div class="message is-danger">
        <div class="message-header">Error</div>
        <div class="message-body">{{$message}}</div>
    </div>
    @enderror
    <div class="field lf-form-sections">
        @foreach($form->sections as $section)
            @include('larafish::forms.section')
        @endforeach
{{--        @each('larafish::forms.section',$form->sections,'section')--}}
    </div>

    @includeWhen($form->requires_recaptcha,'larafish::forms.fields.recaptcha')

    @includeWhen($form->terms,'larafish::forms.fields.terms')

    @if(!empty($form->closing_comments))
        <div class="field content form-builder-closing-comments">
            <p>
                {!! nl2br(e($form->closing_comments)) !!}
            </p>
        </div>
    @endif

    <div class="field">
        <button class="button is-primary">{{$form->submit_button_title}}</button>
    </div>
</form>
    @if($form->has_conditions)
        @singleton('form-assets-conditional')
            @cannot('admin_area')
                @push('footer-scripts')
                    <script defer type="text/javascript" src="{{ mix('js/forms-conditional.js') }}"></script>
                @endpush
            @endcannot
        @endsingleton
    @endif
@else
    <div class="form-builder-submissions-closed">
        <p>{{$form->title}} is not open for submissions.</p>
    </div>
@endcan
