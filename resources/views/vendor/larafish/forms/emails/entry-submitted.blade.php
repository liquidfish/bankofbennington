<h1>{{$title}}</h1>
<p>{!! nl2br(e($description)) !!}</p>
@include('larafish::forms.emails.entry-submitted-receipt')
<p>Download all entries at <a href="{{$route=route('admin.forms.index')}}">{{$route}}</a>.</p>
