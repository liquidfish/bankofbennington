@foreach($fields as [$title,$value])
    <p>
        <strong>{{$title}}</strong>@if(strpos($value,"\n")!==false)<br>
        {!!nl2br(e($value))!!}
        @else{{': '.$value}}@endif
    </p>
@endforeach
@if(!empty($terms))
    <p>
        <input type="checkbox" checked disabled>&nbsp;Check this box as your electronic signature.<br>
        {!! nl2br(e($terms)) !!}
    </p>
@endif
@if(!empty($comments))
    <p>{!! nl2br(e($comments)) !!}</p>
@endif
