@extends('larafish::layouts.blank')

@section('title', 'Login')

@section('content')
    @if(session('message'))
        {{ session('message') }}
    @endif
    <section class="hero is-fullheight">
        <div class="hero-body">
            <form class="container" method="POST" action="{{ URL::route('login') }}">
                <h1 class="is-size-1">Login</h1>
                <div class="field">
                    <label class="label" for="username">Username</label>
                    <div class="control has-icons-left">
                        <input type="text" class="input" id="username" name="username" placeholder="Username">
                        <span class="icon is-small is-left">
                            <i class="mdi mdi-dark mdi-account"></i>
                        </span>
                    </div>
                </div>
                <div class="field">
                    <label class="label" for="password">Password</label>
                    <div class="control has-icons-left">
                        <input type="password" class="input" id="password" name="password" placeholder="password">
                        <span class="icon is-small is-left">
                            <i class="mdi mdi-dark mdi-asterisk"></i>
                        </span>
                    </div>
                </div>

                <input type="submit" value="Login" class="button flex-col is-primary">
                @if($redirect_after)
                    <input type="hidden" name="redirect_after" value="{{$redirect_after}}">
                @endif
                {{ csrf_field() }}
            </form>
        </div>
    </section>
@endsection