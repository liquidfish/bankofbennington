@extends('larafish::layouts.blank')

@section('title', 'Reset Password')

@section('content')
	@if(session('message'))
		{{ session('message') }}
	@endif

	<h1>Reset Password</h1>
	<form action="{{url()->route('password-reset', $code)}}" method="post">
		@csrf
		<div class="field">
			<label class="label" for="password_field">Password</label>
			<div class="control">
				<input class="input" type="password" id="password_field" name="password">
			</div>
			@error('password')
			<p class="help has-text-danger">{{ $message }}</p>
			@enderror
		</div>
		<div class="field">
			<label class="label" for="password_confirmed">Confirm Password</label>
			<div class="control">
				<input class="input" type="password" id="password_confirmed" name="password_confirmation">
			</div>
		</div>
		<button type="submit">Save new password</button>
	</form>
@endsection