@push('meta-tags')
    @foreach ($meta_tags as $meta)
        <meta @if(!empty($meta['name'])) name="{{ $meta['name'] }}" @endif @if(!empty($meta['property'])) property="{{ $meta['property'] }}" @endif content="{{ $meta['value'] }}">
    @endforeach
@endpush