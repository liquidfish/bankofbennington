@if(app()->environment('production','staging'))
    @criticalcss('home')
@endif
<noscript>
    <link rel="stylesheet" href="@can('admin_area'){{ asset('css/admin.css') }}@else{{ asset('css/app.css') }}@endcan">
</noscript>
<script>
    /** LOAD CSS Files **/
    !function(e){"use strict";var n=function(n,t,o){var l,r=e.document,i=r.createElement("link");if(t)l=t;else{var a=(r.body||r.getElementsByTagName("head")[0]).childNodes;l=a[a.length-1]}var d=r.styleSheets;i.rel="stylesheet",i.href=n,i.media="only x",l.parentNode.insertBefore(i,t?l:l.nextSibling);var f=function(e){for(var n=i.href,t=d.length;t--;)if(d[t].href===n)return e();setTimeout(function(){f(e)})};return i.onloadcssdefined=f,f(function(){i.media=o||"all"}),i};"undefined"!=typeof module?module.exports=n:e.loadCSS=n}("undefined"!=typeof global?global:this);
    loadCSS('@can('admin_area'){{ asset('css/admin.css') }}@else{{ asset('css/app.css') }}@endcan');
</script>
{{-- Fonts --}}
<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Titillium+Web:wght@400;600;700&display=swap" rel="stylesheet">
@stack('styles')