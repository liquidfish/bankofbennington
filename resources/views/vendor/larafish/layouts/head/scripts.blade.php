@can('admin_area')
    <script defer type="text/javascript" src="{{ mix('js/app.js') }}"></script>
    <script defer type="text/javascript" src="{{ mix('js/admin.js') }}"></script>
    @stack('admin-scripts')
@else
    <script defer type="text/javascript" src="{{ mix('js/app.js') }}"></script>
@endcan
@stack('scripts')
