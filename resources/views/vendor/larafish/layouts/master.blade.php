@extends('larafish::layouts.frame')
@section('body-content')
    @include('navigation')
    <main id="app" class="bg-bank-base-light">
        <div class="md:max-w-1-5xl mx-auto md:px-12 px-4 md:py-10 py-6">
            @can('admin_area')
                @include('larafish::admin.navigation')
            @endcan
            <div class="{{Request::is('/') ? '' : 'content-page-wrapper'}}">
                @yield('content')
            </div>
        </div>
        @include('components.login-modal')
        @include('components.speed-bump')
    </main>
    @include('footer')
@endsection