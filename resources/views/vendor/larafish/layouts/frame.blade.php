<!DOCTYPE html>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <title>@yield('title') | Bank Of Bennington</title>

    {{--  Open Graph Tags  --}}
    <meta property="og:url" content="{{ url()->current() }}"/>
    <meta property="og:type" content="{{ \Str::contains(url()->current(), 'news') ? 'article' : 'website' }}"/>
    <meta property="og:title" content="@yield('title') | Bank of Bennington"/>
    <meta property="og:image" content="/img/logo.jpg"/>

    {{--  Twitter Tags  --}}
    <meta name="twitter:card" content="summary"/>

    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="theme-color" content="#4285f4">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="canonical" href="{{ url()->current() }}"/>

    {{-- Manually import material design icons --}}
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/MaterialDesign-Webfont/6.3.95/css/materialdesignicons.min.css"
          integrity="sha512-RucK2LvgjtCtrPnR8yAh7tSXcjcCDDiJydP1uS1d4QOVJa+9mFHhS9jwZTG5HLr1FpqEjXER6ua9JB2cnn/LFA=="
          crossorigin="anonymous" referrerpolicy="no-referrer"/>


    @stack('meta-tags')

    @include('larafish::layouts.head.styles')

    @include('larafish::layouts.head.scripts')
    @if(!empty($tagKey = config('services.google.tag_key')))
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id={{$tagKey}}"></script>
        <script>
            window.dataLayer = window.dataLayer || [];

            function gtag() {
                dataLayer.push(arguments);
            }

            gtag('js', new Date());

            gtag('config', '{{$tagKey}}');
        </script>
    @endif
    <!-- Google Tag Manager -->
    <script>(function (w, d, s, l, i) {
            w[l] = w[l] || [];
            w[l].push({
                'gtm.start':
                    new Date().getTime(), event: 'gtm.js'
            });
            var f = d.getElementsByTagName(s)[0],
                j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';
            j.async = true;
            j.src =
                'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
            f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-PM7TDPCP');</script>
    <!-- End Google Tag Manager -->
</head>
<body id="page-content" class="@stack('body_classes') page-content overflow-hidden">
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe
        src="https://www.googletagmanager.com/ns.html?id=GTM-PM7TDPCP"
        height="0"
        width="0"
        style="display:none;visibility:hidden"
    ></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->
@section('body-content')
    test blade content
@show
@stack('footer-scripts')
<script>
    window.appUrl = '{{ env('APP_URL') }}';
</script>
</body>
</html>
