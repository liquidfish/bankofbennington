@extends('larafish::layouts.frame')
@push('body_classes',' larafish-admin ')
@section('body-content')
    <main id="app" class="larafish">
        @include('navigation')
        @include('larafish::admin.navigation')
        @include('larafish::admin.message')
        @yield('content')
        @include('components.login-modal')
    </main>
@endsection
