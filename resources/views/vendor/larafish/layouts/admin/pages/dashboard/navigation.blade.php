<aside class="column is-one-fifth">
    <b-menu class="is-medium">
        <b-menu-list>
            <b-menu-item icon="web" href="/" label="Home"></b-menu-item>
            <b-menu-item icon="speedometer" href="/admin" label="Dashboard"></b-menu-item>
            <b-menu-item icon="account-circle" label="Users">
                <b-menu-item icon="format-list-bulleted" href="/admin/users" label="Manage"></b-menu-item>
                <b-menu-item icon="account-group" href="/admin/roles" label="Roles"></b-menu-item>
                <b-menu-item icon="account-key" href="/admin/permissions" label="Permissions"></b-menu-item>
            </b-menu-item>
            <b-menu-item icon="book-multiple" label="Pages">
                <b-menu-item icon="book" href="/admin/pages" label="Manage"></b-menu-item>
                <b-menu-item icon="layers" href="/admin/components" label="Components"></b-menu-item>
            </b-menu-item>
            <b-menu-item icon="file-multiple" href="/admin/files" label="File Manager"></b-menu-item>
        </b-menu-list>
    </b-menu>

{{--        <admin-link :data="{--}}
{{--            text: 'File Manager',--}}
{{--            location: '/admin/files',--}}
{{--            icon: 'fas fa-file',--}}
{{--        }">--}}
{{--        </admin-link>--}}
{{--    </nav>--}}
</aside>