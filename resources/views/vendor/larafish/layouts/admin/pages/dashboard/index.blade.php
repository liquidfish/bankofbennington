@php
    $admin = true;
@endphp
@extends('larafish::layouts.frame')

@section('title', 'Dashboard')

@section('body-content')
    <div id="app" class="columns">
        @include('larafish::layouts.admin.pages.dashboard.navigation')
        <div class="column">
            <alert></alert>
            <main>
                @yield('content')
            </main>
        </div>
    </div>
@endsection()