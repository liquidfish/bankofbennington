<div id="component_{{$component->id}}" class="content" @can('update',$component) is="lfFormDisplay" :component-id="{{$component->id}}"
        @if(!empty($initFormId = $component->pageData['formId'])) :initial-form-id="{{$initFormId}}" @endif @endcan>
    @includeWhen(!empty($component->pageData['form']),'larafish::forms.form',['form'=>$component->pageData['form']??false])
    @if(empty($component->pageData['form']))
        <h1 class="is-size-1">- No form selected -</h1>
    @endif
</div>