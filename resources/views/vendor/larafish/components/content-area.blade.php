<div id="component_{{$component->id}}" class="content has-top-border" @can('update',$component) is="lfContentArea" :component-id="{{$component->id}}" @endcan >
    <div v-pre>{!! $component->pageData['text'] !!}</div>
</div>