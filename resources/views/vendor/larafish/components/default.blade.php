<div id="component_{{$component->id}}" data-component="{{$component->id}}" class="container">
    @foreach($component->bowls() as $bowl=>$components)
        @include('larafish::components.bowl')
    @endforeach
</div>