<div id="bowl_{{$component->id}}_{{$bowl}}" @can('update',$page) is="lfBowl" :bowl-name="'{{$bowl}}'" @endcan >
    @foreach($components as $component)
        @includeFirst($component->view)
    @endforeach
</div>