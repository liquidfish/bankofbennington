<div id="component_{{$component->id}}"
     class="has-top-border"
     {!! $editable?'is="lfComponentBoundary"':''!!}
     data-component="{{$component->id}}">
    <div class="flex md:flex-row flex-col-reverse gap-7">
        <div style="flex:1 1 50%">
            @bowl('main')
        </div>
        <div style="flex:1 1 50%">
            @bowl('second')
            @include('larafish::components.bowl',['bowl'=>'column2','components'=>$component->bowls('column2')])
        </div>
    </div>
</div>