<!DOCTYPE html>
<html class="no-js" lang="en">
<head>
    @include('larafish::backend.layouts.parts.head.meta-top')

    <title>Larafish - @yield('title')</title>

    @include('larafish::backend.layouts.parts.head.meta-bottom')
</head>
<body class=" @stack('body_classes') ">
<div id="app">
@section('body-content')
    test
@show
</div>

@include('larafish::backend.layouts.parts.foot.tags')
</body>
</html>
