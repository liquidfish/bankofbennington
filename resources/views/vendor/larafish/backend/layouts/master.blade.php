@extends('larafish::backend.layouts.frame')
@section('body-content')
    @include('larafish::backend.layouts.parts.navigation')

    <alert></alert>

    <main>
        @yield('content')
    </main>
    @include('larafish::backend.layouts.parts.foot')
@endsection