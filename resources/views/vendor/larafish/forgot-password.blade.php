@extends('larafish::layouts.blank')

@section('title', 'Forgot Password')

@section('content')
    @if(session('message'))
        {{ session('message') }}
    @endif

    <h1>Forgot Password</h1>
    <form action="{{url()->route('password-reset-request')}}" method="post">
        @csrf
        <div class="field">
            <label class="label" for="username">Username</label>
            <div class="control">
                <input class="input" type="email" name="username" placeholder="email@example.com">
            </div>
        </div>
        <button type="submit">Email reset link</button>
    </form>
@endsection