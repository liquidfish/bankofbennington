@extends('larafish::layouts.master')

@section('title', '"'.$query.'" search results')
@push('body_classes', 'larafish-search')

@section('content')
	<section class="container">
		<h1 class="is-size-1">Search Results for "{{$query}}"</h1>
		@if($results->isEmpty())
			<p>No results found</p>
		@else
			@foreach($results as $result)
				<div>
					<a class="is-size-3" href="{{$result->version->page->path}}#component_{{$result->id}}" class="">{{$result->version->title}}</a>
					<p>{!! $result->content !!}</p>
				</div>
			@endforeach
		@endif
	</section>
@endsection