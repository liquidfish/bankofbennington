@extends('larafish::layouts.admin.master')

@section('title', 'Home Card Creation')

@section('content')
    <section class="container-bank-xl mx-auto box has-margin-top-size-3 has-margin-bottom-size-3">
        <header class="level">
            <div class="level-left">
                <div class="level-item">
                    <h1 class="is-size-1">Home Card Creation</h1>
                </div>
            </div>
            <div class="level-right">
                <div class="level-item">
                    <a href="{{ URL::route('admin.home-cards.index',[],false) }}" title="View All Home Cards" class="button is-primary is-family-primary"><b-icon icon="bookmark-multiple"></b-icon><span>View All Home Cards</span></a>
                </div>
            </div>
        </header>
        <form enctype="multipart/form-data" action="{{ URL::route('admin.home-cards.store') }}" method="POST">
            @csrf
            <div class="field">
                <label for="title" class="label">Title</label>
                <input required type="text" placeholder="Title" name="title" class="input">
            </div>
            <div class="field">
                <label for="content" class="label">Content</label>
                <textarea name="content" id="content" class="textarea" placeholder="Content goes here..."></textarea>
            </div>
            <div class="columns">
                <div class="column">
                    <div class="field">
                        <label for="button_text" class="label">Button Text</label>
                        <input required type="text" placeholder="Button Text" name="button_text" class="input">
                    </div>
                </div>
                <div class="column">
                    <div class="field">
                        <label for="button_url" class="label">Button URL</label>
                        <input required type="text" placeholder="Button URL" name="button_url" class="input">
                    </div>
                </div>
            </div>
            <div class="columns">
                <div class="column">
                    <div class="field">
                        <label for="image" class="label">Card Image (430x240)</label>
                        <input type="file" accept=".jpg, .jpeg .png" name="image" />
                    </div>
                </div>
            </div>
            <div class="columns">
                <div class="column">
                    <div class="field">
                        <label for="Image Alt Text" class="label">Card Image Alt Text</label>
                        <input type="text" name="alt_text" class="input">
                    </div>
                </div>
                <div class="column"></div>
            </div>
            <div class="field">
                <label for="active" class="label">Visible</label>
                <label for="active">
                    <input id="active" type="checkbox" name="active">
                    Yes
                </label>
            </div>

            <hr>

            <div class="field">
                <div class="control">
                    <button type="submit" class="button is-primary">Create</button>
                </div>
            </div>
        </form>
    </section>
@endsection