@extends('larafish::layouts.admin.master')

@section('title', 'Home Card Editing')

@section('content')
    <section class="container-bank-xl mx-auto box has-margin-top-size-3 has-margin-bottom-size-3">
        <header class="level">
            <div class="level-left">
                <div class="level-item">
                    <h1 class="is-size-1">Home Card Editing</h1>
                </div>
            </div>
            <div class="level-right">
                <div class="level-item">
                    <a href="{{ URL::route('admin.home-cards.index',[],false) }}" title="View All Home Cards" class="button is-primary is-family-primary"><b-icon icon="bookmark-multiple"></b-icon><span>View All Home Cards</span></a>
                </div>
            </div>
        </header>
        <div class="columns">
            <div class="column">
                <form action="{{ URL::route('admin.home-cards.update', $card->id, false) }}" method="post" enctype="multipart/form-data">
                    @csrf
                    @method('patch')
                    <div class="field">
                        <label for="title" class="label">Title</label>
                        <input required type="text" placeholder="Title" name="title" class="input" value="{{$card->title}}">
                    </div>
                    <div class="field">
                        <label for="content" class="label">Content</label>
                        <textarea name="content" id="content" class="textarea" placeholder="Content goes here...">{{$card->content}}</textarea>
                    </div>
                    <div class="columns">
                        <div class="column">
                            <div class="field">
                                <label for="button_text" class="label">Button Text</label>
                                <input required type="text" placeholder="Button Text" value="{{$card->button_text}}" name="button_text" class="input">
                            </div>
                        </div>
                        <div class="column">
                            <div class="field">
                                <label for="button_url" class="label">Button URL</label>
                                <input required type="text" placeholder="Button URL" value="{{$card->button_url}}" name="button_url" class="input">
                            </div>
                        </div>
                    </div>
                    <div class="columns">
                        <div class="column">
                            <div class="field">
                                <label for="image" class="label">Card Image (430x240)</label>
                                <input type="file" accept=".jpg, .jpeg .png" name="image" />
                                @if($card->photo)
                                    <img class="mt-4" src="{{$card->photo}}" />
                                @endif
                            </div>
                        </div>
                        <div class="column">
                            <div class="field">
                                <label for="Image Alt Text" class="label">Card Image Alt Text</label>
                                <input type="text" name="alt_text" class="input" value="{{$card->alt_text}}">
                            </div>
                        </div>
                    </div>
                    <div class="field">
                        <label for="active" class="label">Visible</label>
                        <label for="active">
                            <input id="active" type="checkbox" name="active" {{$card->active ? 'checked="checked"' : ''}}>
                            Yes
                        </label>
                    </div>

                    <hr>

                    <div class="field">
                        <div class="control">
                            <button type="submit" class="button is-primary">Update</button>
                        </div>
                    </div>
                </form>
            </div>

            <div class="column is-one-third">
                <form method="post" action="{{URL::route('admin.home-cards.destroy',$card->id)}}" class="form box is-danger">
                    <div class="field">
                        <label class="label has-text-danger" for="confirm_delete">Delete Home Card</label>
                        <p class="control">
                            <input class="input" type="text" id="confirm_delete" name="confirm_delete" placeholder="DELETE" required>
                        </p>
                        <p class="help has-text-danger">Type "DELETE" to confirm deletion.</p>
                    </div>
                    <div class="control">
                        <button type="submit" class="button is-danger">Delete Home Card</button>
                    </div>
                    @method('DELETE')
                    @csrf
                </form>
            </div>
        </div>
    </section>
@endsection