@extends('larafish::layouts.admin.master')

@section('title', 'Announcement Editing')

@section('content')
    <section class="container-bank-xl mx-auto box has-margin-top-size-3 has-margin-bottom-size-3">
        <header class="level">
            <div class="level-left">
                <div class="level-item">
                    <h1 class="is-size-1">Announcement Editing</h1>
                </div>
            </div>
        </header>
        <div class="columns">
            <div class="column">
                <form method="post" action="{{ URL::route('admin.announcements.update',[$announcement->id],false) }}">
                    @csrf
                    @method('patch')
                    <div class="field">
                        <label for="message" class="label">Announcement Message</label>
                        <textarea name="message" id="message" cols="30" rows="10" class="textarea">{{$announcement->message}}</textarea>
                    </div>
                    <div class="field">
                        <label for="enabled" class="label">Visible</label>
                        <label for="enabled">
                            <input id="enabled" type="checkbox" name="enabled" {{$announcement->enabled ? 'checked="checked"' : ''}}>
                            Yes
                        </label>
                    </div>
                    <div class="field">
                        <div class="control">
                            <button type="submit" class="button is-primary">Update</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section>
@endsection