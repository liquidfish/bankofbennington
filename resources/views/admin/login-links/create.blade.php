@extends('larafish::layouts.admin.master')

@section('title', 'Login Link Creation')

@section('content')
    <section class="container-bank-xl mx-auto box has-margin-top-size-3 has-margin-bottom-size-3">
        <header class="level">
            <div class="level-left">
                <div class="level-item">
                    <h1 class="is-size-1">Login Link Creation</h1>
                </div>
            </div>
            <div class="level-right">
                <div class="level-item">
                    <a href="{{ URL::route('admin.login-links.index',[],false) }}" title="View All Login Links" class="button is-primary is-family-primary"><b-icon icon="bookmark-multiple"></b-icon><span>View All Login Links</span></a>
                </div>
            </div>
        </header>
        <form action="{{ URL::route('admin.login-links.store') }}" method="POST">
            @csrf
            <div class="columns">
                <div class="column">
                    <div class="field">
                        <label for="button_text" class="label">Button Text</label>
                        <input required type="text" placeholder="Button Text" name="button_text" class="input">
                    </div>
                </div>
                <div class="column">
                    <div class="field">
                        <label for="button_url" class="label">Button URL</label>
                        <input required type="text" placeholder="Button URL" name="button_url" class="input">
                    </div>
                </div>
            </div>
            <div class="field">
                <label for="active" class="label">Visible</label>
                <label for="active">
                    <input id="active" type="checkbox" name="active">
                    Yes
                </label>
            </div>

            <hr>

            <div class="field">
                <div class="control">
                    <button type="submit" class="button is-primary">Create</button>
                </div>
            </div>
        </form>
    </section>
@endsection