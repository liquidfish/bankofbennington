@extends('larafish::layouts.admin.master')

@section('title', 'Footer Editing')

@section('content')
    <section class="container-bank-xl mx-auto box has-margin-top-size-3 has-margin-bottom-size-3">
        <header class="level">
            <div class="level-left">
                <div class="level-item">
                    <h1 class="is-size-1">Footer Editing</h1>
                </div>
            </div>
        </header>
        <div class="columns">
            <div class="column">
                <form method="post" action="{{ URL::route('admin.footer.update',[$footer->id],false) }}">
                    @csrf
                    @method('patch')
                    <div class="field">
                        <label for="content" class="label">Footer Message</label>
                        <textarea name="content" id="content" cols="30" rows="10" class="textarea">{{$footer->content}}</textarea>
                    </div>
                    <div class="field">
                        <div class="control">
                            <button type="submit" class="button is-primary">Update</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section>
@endsection