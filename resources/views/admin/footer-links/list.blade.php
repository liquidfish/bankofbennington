@extends('larafish::layouts.admin.master')

@section('title', 'Footer Links Manager')

@section('content')
    <section class="container-bank-xl mx-auto box has-margin-top-size-3">
        <header>
            <div class="level">
                <div class="level-left">
                    <div class="level-item">
                        <h1 class="is-size-1">Footer Links Manager</h1>
                    </div>
                    <p>(Drag rows to sort priority)</p>
                </div>
                <div class="level-right">
                    <div class="level-item">
                        <a href="{{route('admin.footer-links.create')}}" class="button is-primary">
                            <span class="icon"><i class="mdi mdi-bookmark-plus"></i></span>
                            <span>New Footer Link</span>
                        </a>
                    </div>
                </div>
            </div>
        </header>
        <footer-link-table :links='@json($links)' />
    </section>
@endsection
