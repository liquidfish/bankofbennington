@extends('larafish::layouts.admin.master')

@section('title', 'Footer Link Editing')

@section('content')
    <section class="container-bank-xl mx-auto box has-margin-top-size-3 has-margin-bottom-size-3">
        <header class="level">
            <div class="level-left">
                <div class="level-item">
                    <h1 class="is-size-1">Footer Link Editing</h1>
                </div>
            </div>
            <div class="level-right">
                <div class="level-item">
                    <a href="{{ URL::route('admin.footer-links.index',[],false) }}" title="View All Footer Links" class="button is-primary is-family-primary"><b-icon icon="bookmark-multiple"></b-icon><span>View All Footer Links</span></a>
                </div>
            </div>
        </header>
        <div class="columns">
            <div class="column">
                <form action="{{ URL::route('admin.footer-links.update', $link->id, false) }}" method="post" enctype="multipart/form-data">
                    @csrf
                    @method('patch')
                    <div class="field">
                        <label for="title" class="label">Title</label>
                        <input required type="text" placeholder="Title" name="title" class="input" value="{{$link->title}}">
                    </div>
                    <div class="field">
                        <label for="content" class="label">Content</label>
                        <textarea name="content" id="content" class="textarea" placeholder="Content goes here...">{{$link->content}}</textarea>
                    </div>
                    <div class="columns">
                        <div class="column">
                            <div class="field">
                                <label for="button_text" class="label">Button Text</label>
                                <input required type="text" placeholder="Button Text" value="{{$link->button_text}}" name="button_text" class="input">
                            </div>
                        </div>
                        <div class="column">
                            <div class="field">
                                <label for="button_url" class="label">Button URL</label>
                                <input required type="text" placeholder="Button URL" value="{{$link->button_url}}" name="button_url" class="input">
                            </div>
                        </div>
                    </div>
                    <div class="columns">
                        <div class="column">
                            <div class="field">
                                <label for="image" class="label">Footer Link Image</label>
                                <input type="file" name="image" />
                            </div>
                        </div>
                        @if($link->photo)
                            <div class="column">
                                <img src="{{$link->photo}}" />
                            </div>
                        @endif
                    </div>
                    <div class="field">
                        <label for="active" class="label">Visible</label>
                        <label for="active">
                            <input id="active" type="checkbox" name="active" {{$link->active ? 'checked="checked"' : ''}}>
                            Yes
                        </label>
                    </div>

                    <hr>

                    <div class="field">
                        <div class="control">
                            <button type="submit" class="button is-primary">Update</button>
                        </div>
                    </div>
                </form>
            </div>

            <div class="column is-one-third">
                <form method="post" action="{{URL::route('admin.footer-links.destroy',$link->id)}}" class="form box is-danger">
                    <div class="field">
                        <label class="label has-text-danger" for="confirm_delete">Delete Footer Link</label>
                        <p class="control">
                            <input class="input" type="text" id="confirm_delete" name="confirm_delete" placeholder="DELETE" required>
                        </p>
                        <p class="help has-text-danger">Type "DELETE" to confirm deletion.</p>
                    </div>
                    <div class="control">
                        <button type="submit" class="button is-danger">Delete Footer Link</button>
                    </div>
                    @method('DELETE')
                    @csrf
                </form>
            </div>
        </div>
    </section>
@endsection