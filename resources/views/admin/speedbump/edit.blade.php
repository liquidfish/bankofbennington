@extends('larafish::layouts.admin.master')

@section('title', 'Speed Bump Notification Editing')

@section('content')
    <section class="container-bank-xl mx-auto box has-margin-top-size-3 has-margin-bottom-size-3">
        <header class="level">
            <div class="level-left">
                <div class="level-item">
                    <h1 class="is-size-1">Speed Bump Notification Editing</h1>
                </div>
            </div>
        </header>
        <div class="columns">
            <div class="column">
                <edit-speed-bump :whitelist='@json($whitelist)' />
            </div>
        </div>
    </section>
@endsection