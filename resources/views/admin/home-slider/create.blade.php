@extends('larafish::layouts.admin.master')

@section('title', 'Slide Creation')

@section('content')
    <section class="container-bank-xl mx-auto box has-margin-top-size-3 has-margin-bottom-size-3">
        <header class="level">
            <div class="level-left">
                <div class="level-item">
                    <h1 class="is-size-1">Slide Creation</h1>
                </div>
            </div>
            <div class="level-right">
                <div class="level-item">
                    <a href="{{ URL::route('admin.home-slider.index',[],false) }}" title="View All Slides" class="button is-primary is-family-primary"><b-icon icon="bookmark-multiple"></b-icon><span>View All Slides</span></a>
                </div>
            </div>
        </header>
        <edit-slider-form :page="'Create'"
              method="post"
              enctype="multipart/form-data"
              action="{{ URL::route('admin.home-slider.store',[],false) }}"
              @if($errors->isNotEmpty()) :errors='@json($errors->getMessages())' @endif>
            @csrf
        </edit-slider-form>
    </section>
@endsection