@extends('larafish::layouts.admin.master')

@section('title', 'Slide Editing')

@section('content')
    <section class="container-bank-xl mx-auto box has-margin-top-size-3 has-margin-bottom-size-3 home-slider">
        <header class="level">
            <div class="level-left">
                <div class="level-item">
                    <h1 class="is-size-1">Slide Editing</h1>
                </div>
            </div>
            <div class="level-right">
                <div class="level-item">
                    <a href="{{ URL::route('admin.home-slider.index',[],false) }}" title="View All Slides" class="button is-primary is-family-primary"><b-icon icon="bookmark-multiple"></b-icon><span>View All Slides</span></a>
                </div>
            </div>
        </header>
        <div class="columns">
            <div class="column">
                <edit-slider-form :page="'Update'"
                      method="post"
                      enctype="multipart/form-data"
                      action="{{ URL::route('admin.home-slider.update',$slide->id,false) }}"
                      :slide='@json($slide)'
                      @if($errors->isNotEmpty()) :errors='@json($errors->getMessages())' @endif>
                      @csrf
                      @method('patch')
                </edit-slider-form>
            </div>

            <div class="column is-one-third">
                <form method="post" action="{{URL::route('admin.home-slider.destroy',$slide->id)}}" class="form box is-danger">
                    <div class="field">
                        <label class="label has-text-danger" for="confirm_delete">Delete Slide</label>
                        <p class="control">
                            <input class="input" type="text" id="confirm_delete" name="confirm_delete" placeholder="DELETE" required>
                        </p>
                        <p class="help has-text-danger">Type "DELETE" to confirm deletion.</p>
                    </div>
                    <div class="control">
                        <button type="submit" class="button is-danger">Delete Slide</button>
                    </div>
                    @method('DELETE')
                    @csrf
                </form>
            </div>
        </div>
    </section>
@endsection