<footer class="container-bank-xl mx-auto">
    {{-- Only show on homepage  --}}
    @if(Request::is('/'))
        @include('components.links')
    @endif
    <section class="flex md:flex-row flex-col md:items-center items-start md:p-10 px-4 py-10">
        @if(!Request::is('financial-planning/*'))
            <img src="/img/fdic.png" class="mr-10" alt="Federal Deposit Insurance Corporation Logo">
        @endif
        @if(isset($footer))
            <p class="flex-grow md:mt-0 mt-6 uppercase font-normal leading-6 text-bank-base-mid">
                {{$footer->content}}
            </p>
        @endif
    </section>
    <script> window.addEventListener('load', function() { document.querySelector('[href="https://mtgpro.co/dr/c/8f7m6"]').addEventListener('click', function() { gtag('event', 'conversion', { 'send_to': 'AW-991836444/Z-fOCMjsv5kYEJzy-NgD' }); }) })
    </script>
</footer>
