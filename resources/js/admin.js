/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

import SwiperSlider from './SwiperSlider';

window.addEventListener('load', () => {
    if (document.querySelector('.swiper-container')) {
        window.SwiperSlider = new SwiperSlider();
    }
}, false);

// require('./utilities/element-closest-polyfill');
//
// window._ = require('lodash');
//
// window._.mixin({
//     start:function(string,prefix){return _.startsWith(string,prefix)?string:prefix+string},
//     finish:function(string,suffix){return _.endsWith(string,suffix)?string:string+suffix},
//     arrayWrap:function(thing){return Array.isArray(thing)?thing:[thing]},
// });
//
// const {Vue, store} = require('./vue').default;
//
// let files = require.context('./components', true, /\.vue$/i);
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));
//
// const app = new Vue({
//     el: '#app',
//     store,
// });
