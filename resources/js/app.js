/**
 * To include vue on the client-side, uncomment the lines below.
 * Any component in the js/components/public folder will be autoloaded, as well as all of buefy.
 *
 * If you don't need vue client-side, well, this is your entry point for whatever code you _do_ need.
 */
import Navigation from './Navigation';
import SwiperSlider from './SwiperSlider';

window.addEventListener('load', () => {
    if (document.querySelector('.navigation-menu')) {
        window.Navigation = new Navigation();
    }
    if (document.querySelector('.swiper-container')) {
        window.SwiperSlider = new SwiperSlider();
    }
}, false);

require('./utilities/element-closest-polyfill');

window._ = require('lodash');

window._.mixin({
    start:function(string,prefix){return _.startsWith(string,prefix)?string:prefix+string},
    finish:function(string,suffix){return _.endsWith(string,suffix)?string:string+suffix},
    arrayWrap:function(thing){return Array.isArray(thing)?thing:[thing]},
});

const {Vue, store} = require('./vue').default;

let files = require.context('./components', true, /\.vue$/i);
files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));

const app = new Vue({
    el: '#app',
    store,
});
