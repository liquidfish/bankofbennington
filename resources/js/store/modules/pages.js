
import { SnackbarProgrammatic as Snackbar } from 'buefy';

const editablePageProperties = ['path','priority','depth','published','redirect_url','title','in_nav'];
const ensureUniquePath = (page, siblings)=>{
    let siblingMatch = uriSegment=>_.some(siblings,sib=>sib.id !== page.id && sib.uriSegment === uriSegment);
    if(siblingMatch(page.uriSegment)){
        let n = 2;
        while(siblingMatch(page.uriSegment+'-'+n)) n++;
        return page.uriSegment+'-'+n;
    }
    return page.uriSegment;
};
const upToLastSlash = (path,includeSlash=true)=>path.slice(0,path.lastIndexOf('/')+(includeSlash?1:0));
const afterLastSlash = (path,includeSlash=false)=>path.slice(path.lastIndexOf('/')+(includeSlash?0:1));
const mapLoadedPage = page=>_.assign(page, {
    original:_.pick(page,editablePageProperties),
    deleted:false,
    changed:false,
    exists:true,
    open:false,
    uriSegment: afterLastSlash(page.path),
    root: false,
    childrenLoaded: false,
    loading:false,
});
const mapPageForSave = page=>{
    let data = {
        ..._.pick(page,['id','exists','deleted']),
        ..._(page).pick(editablePageProperties)
            .pickBy((value,key)=>value !== page.original[key])
            .value()
    };
    if(!_.isUndefined(data.title)){
        data.nav_title = data.title;
        delete data.title;
    }
    if(!_.isUndefined(data.path)){
        data.uri = data.path.slice(1) || '/';
        delete data.path;
    }
    return data;
};

export default {
    namespaced: true,
    state: {
        addingPage: false,
        saving: false,
        clearingCache: false,
        pages: []
    },
    mutations: {
        loadPages(state, {pages}){
            let root = _.assign(mapLoadedPage(_.omit(pages,'children')),{root:true,childrenLoaded:true,open:true});
            state.pages = [root,..._.map(pages.children,mapLoadedPage)];
        },
        loadMorePages(state, pages){
            let parent = _.find(state.pages,{id: pages.id});
            parent.childrenLoaded = true;
            state.pages.push(..._.map(pages.children,mapLoadedPage));
        },
        addPage(state, page){
            state.pages.push(page);
        },
        updatePage(state,page){
            let pageRecord = _.find(state.pages,{id:page.id});
            _.assign(pageRecord,_.pick(page,[...editablePageProperties,'depth','deleted','open','parentId','uriSegment','loading']));
            pageRecord.changed = pageRecord.exists === pageRecord.deleted ||
                _.some(editablePageProperties,prop=>pageRecord[prop] !== pageRecord.original[prop]);
        },
        loading(state,load){
            _.assign(state,_.pick(load,['addingPage','saving','clearingCache']));
        }
    },
    actions: {
        addPage({state,commit,getters},{parent,title}){
            commit('loading',{addingPage:true});
            let newPage = {
                id: _.uniqueId('new_'),
                title,
                original:_.mapValues(_.keyBy(editablePageProperties),()=>null),
                published:false,
                exists: false,
                changed: true,
                deleted: false,
                open: false,
                parentId:parent.id,
                permissions:{...parent.permissions,delete:true},
            };
            axios.get('/admin/pages/check-uri?title='+encodeURIComponent(title))
                .then(response => {
                    if(response.data){
                        newPage.uriSegment = response.data;
                    }
                })
                .catch(error => {
                    Snackbar.open({
                        message: 'Failed to generate URI from title!',
                        type: 'is-warning',
                        position: 'is-top',
                        indefinite: false,
                    });
                    console.log(error);
                    newPage.uriSegment = 'new-page';
                })
                .finally(()=>{
                    parent = getters.page(parent.id);
                    let parentSubpages = getters.subpages(parent.id);
                    _.assign(newPage, {
                        uriSegment: ensureUniquePath(newPage, parentSubpages),
                        depth: parent.depth + 1,
                        priority:parentSubpages.length,
                    });
                    newPage.path = _.finish(parent.path, '/') + newPage.uriSegment;
                    commit('addPage',newPage);
                    commit('loading',{addingPage:false});
                });
        },
        updatePagePath({state,commit,getters,dispatch},page){
            page.uriSegment = ensureUniquePath(page,getters.subpages(page.parentId));
            commit('updatePage',{..._.pick(page,['id','uriSegment']),path:upToLastSlash(page.path)+page.uriSegment});
            let children = getters.subpages(page.id);
            _.each(children,subpage=>dispatch('updatePageParent',{page:subpage,parent:getters.page(page.id),priority:subpage.priority,siblings:children}));
        },
        updatePageParent({state,commit,getters,dispatch},{page,parent,priority,siblings}){
            if(siblings===undefined) siblings = getters.subpages(parent.id);
            siblings = _.reject(siblings,{id:page.id});
            let uriSegment = ensureUniquePath(page,siblings);
            commit('updatePage',{
                id:page.id,
                uriSegment,
                priority: priority===undefined?siblings.length:priority,
                path: _.finish(parent.path, '/') + uriSegment,
                depth: parent.depth + 1,
                parentId: parent.id,
            });
            if(priority!==undefined){
                _.each(_.sortBy(_.filter(siblings,sib=>sib.priority>=priority),'priority'),
                    sib=>commit('updatePage',{id:sib.id,priority:++priority})
                );
            }
            let children = getters.subpages(page.id);
            _.each(children,subpage=>dispatch('updatePageParent',{page:subpage,parent:getters.page(page.id),priority:subpage.priority,siblings:children}));
        },
        decreaseDepth({state,commit,getters,dispatch},page){
            let parent = getters.page(page.parentId);
            let grandParent = getters.page(parent.parentId);
            dispatch('updatePageParent',{page:{...page,priority:grandParent},parent:grandParent});
        },
        clearPageCache({commit,dispatch}){
            commit('loading',{clearingCache:true});
            axios.post('/admin/pages/clear-cache')
                .then(() => {
                    Snackbar.open({
                        message: 'Page cache cleared!',
                        type: 'is-success',
                        position: 'is-bottom',
                        indefinite: false,
                    });
                    dispatch('reFetchPages');
                })
                .catch(error => {
                    Snackbar.open({
                        message: 'Error clearing page cache!',
                        type: 'is-danger',
                        position: 'is-top',
                        indefinite: false,
                    });
                    console.log('clearCacheError',error);
                })
                .finally(()=>commit('loading',{clearingCache:false}));
        },
        saveChanges({state, commit, getters}){
            commit('loading',{saving:true});
            let pages = _.map(
                _.filter(state.pages,'changed'),
                mapPageForSave
            );
            console.log('pagesSaving',pages);
            axios.post('/admin/pages/update',{pages})
                .then(response => {
                    console.log('saveResponse',response);
                    commit('loadPages',{pages:response.data});
                    Snackbar.open({
                        message: 'Page changes saved!',
                        type: 'is-success',
                        position: 'is-bottom',
                        indefinite: false,
                    });

                })
                .catch(error => {
                    Snackbar.open({
                        message: 'Error saving page changes! Server response issue.',
                        type: 'is-danger',
                        position: 'is-top',
                        indefinite: false,
                    });
                    console.log('saveError',error);
                })
                .finally(()=>commit('loading',{saving:false}));
        },
        reFetchPages({state, commit,getters,dispatch}){
            let root = _.find(state.pages,{root:true});
            axios.get('/admin/pages/'+root.id+'/branch')
                .then(response => {
                    if(response.data){
                        commit('loadPages',{pages:response.data});
                        Snackbar.open({
                            message: 'Pages Reloaded!',
                            type: 'is-success',
                            position: 'is-bottom',
                            indefinite: false,
                        });
                    }
                })
                .catch(error => {
                    Snackbar.open({
                        message: 'Failed to load pages!',
                        type: 'is-warning',
                        position: 'is-top',
                        indefinite: false,
                    });
                    console.log(error);
                });
        },
        fetchChildren({commit,getters,dispatch},{parentId}){
            let parent = getters.page(parentId);
            if(!parent.childrenLoaded && parent.exists){
                commit('updatePage',{id:parentId,loading:true});
                axios.get('/admin/pages/'+parentId+'/branch')
                    .then(response => {
                        if(response.data){
                            commit('loadMorePages',response.data);
                            if(parent.path !== parent.original.path){
                                let children = getters.subpages(parent.id);
                                _.each(children,subpage=>dispatch('updatePageParent',{page:subpage,parent,priority:subpage.priority,siblings:children}));
                            }
                        }
                    })
                    .catch(error => {
                        Snackbar.open({
                            message: 'Failed to load child pages!',
                            type: 'is-warning',
                            position: 'is-top',
                            indefinite: false,
                        });
                        console.log(error);
                    })
                    .finally(()=>{
                        commit('updatePage',{id:parentId,loading:false});
                    })
                ;
            }
        },
        openPage({commit,dispatch},{open,pageId}){
            if(open){
                dispatch('fetchChildren',{parentId:pageId});
            }
            commit('updatePage',{id:pageId,open});
        },
        deletePage({commit,dispatch,getters},page){
            if(!page.deleted){
                commit('updatePage',{id:page.id,open:false,deleted:true});
                let children = getters.subpages(page.id);
                _.each(children,child=>dispatch('deletePage',child));
            }
        }
    },
    getters: {
        page(state){
            return id=>_.find(state.pages,{id});
        },
        subpages(state){
            return parentId=>_.sortBy(_.filter(state.pages, {parentId}),'priority');
        },
        pageIsLoading(state){
            return id=>_.find(state.loadingPages,{id});
        }
    }
};