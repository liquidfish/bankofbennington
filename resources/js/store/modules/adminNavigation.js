export default {
    namespaced: true,
    state: {
        widgets: [],
    },
    mutations: {
        addWidget(state,{name,element,prepend}){
            if(prepend){
                state.widgets.unshift({name,element});
            } else {
                state.widgets.push({name,element});
            }
        },
        removeWidget(state,name){
            state.widgets = _.reject(state.widgets,{name});
        },
    },
    actions: {

    },
    getters: {

    }
};