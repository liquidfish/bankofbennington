
import { SnackbarProgrammatic as Snackbar } from 'buefy';

let newName = function(length = 8){
    const base = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'.split('');
    let array = new Uint8Array(length);
    let crypto = window.crypto || window.msCrypto;
    crypto.getRandomValues(array);
    return _.map(array,value=>base[value%base.length]).join('');
};
let newConditionGroup = function(operation){
    return {
        id:newName(),
        type: 'group',
        not: false,
        operation,
        conditions:[],
    };
};

export default {
    namespaced: true,
    state: {
        processing:false,
        form: null,
        message: null
    },
    mutations: {
        processing(state,value){
            state.processing = value;
        },
        load(state,form){
            state.form = form;
        },
        addSection(state){
            state.form.sections.push({
                title: '',
                fields : [],
                name: newName(),
            });
        },
        addField(state, {section,type,field}){
            section.fields.push(_.assign({
                type,
                required : true,
                validator : '',
                encrypt : false,
                conditional: false,
                conditions: null,
                name: newName(),
            },_.cloneDeep(field)));
        },
        addOption(state,field) {
            field.options.push({ name : 'My Option', id:newName(3) });
        },
        addInitialFieldCondition(state,field){
            field.conditions = newConditionGroup('and');
        },
        addConditionGroup(state,{group,operation}){
            group.conditions.push(newConditionGroup(operation));
        },
        addConditionField(state,group){
            group.conditions.push({
                id:newName(),
                type: 'field',
                fieldName: null,
                not: false,
                operation:'=',
                value:null,
            });
        },
    },
    actions: {
        load({commit},formDataUrl){
            commit('processing',true);
            axios.get(formDataUrl)
                .then(response => {
                    commit('load',response.data);
                })
                .catch(function (error) {
                    console.log(error);
                    Snackbar.open({
                        message: 'Failed to load form data!',
                        type: 'is-danger',
                        position: 'is-top',
                        indefinite: false,
                    });
                })
                .finally(()=>commit('processing',false))
            ;
        },
        save({state,commit},formSaveUrl){
            commit('processing',true);
            axios.patch(formSaveUrl,state.form)
                .then(response => {
                    commit('load',response.data);
                    Snackbar.open({
                        message: 'Form saved!',
                        type: 'is-success',
                        position: 'is-bottom',
                        indefinite: false,
                    });
                })
                .catch(function (error) {
                    console.log(error);
                    Snackbar.open({
                        message: 'Failed to save form data!',
                        type: 'is-danger',
                        position: 'is-top',
                        indefinite: false,
                    });
                })
                .finally(()=>commit('processing',false))
            ;
        }
    },
    getters: {
        allFields(state){
            return state.form ? _.flatMap(state.form.sections,section=>section.fields) : [];
        },
        field(state,getters){
            return name=>{
                if(!name) return null;
                if(name === '_date') return getters.currentDateField;
                return _.find(getters.allFields,{name});
            }
        },
        fieldTypes(){
            return {
                text: {
                    description: 'Text Field',
                    label: 'Text Field',
                    icon: 'cursor-text'
                },
                checkboxes: {
                    description: 'Checkboxes',
                    label: 'Checkbox Group',
                    options: [],
                    icon: 'checkbox-multiple-marked-outline'
                },
                radios: {
                    description: 'Radios',
                    label: 'Radio Group',
                    options: [],
                    icon: 'radiobox-marked'
                },
                textarea: {
                    description: 'Textarea',
                    label: 'Textarea',
                    icon: 'card-text-outline'
                },
                dropdown: {
                    description: 'Dropdown Field',
                    label: 'Dropdown',
                    options: [],
                    icon: 'chevron-down'
                },
                photo: {
                    description: 'Photo',
                    label: 'Photo',
                    multiple: false,
                    icon: 'file-image-outline'
                },
                file: {
                    description: 'File',
                    label: 'File',
                    multiple: false,
                    icon: 'file-outline'
                },
                date: {
                    description: 'Date Field',
                    label: 'Date Field',
                    icon: 'calendar'
                },
                time: {
                    description: 'Time Field',
                    label: 'Time Field',
                    icon: 'clock-outline'
                },
                ssn: {
                    description: 'Social Security Number',
                    label: 'Social Security Number',
                    encrypt: true,
                    icon: 'lock-outline'
                },
            }
        },
        operatorIsRange(){
            return condition=>condition.operation === 'between';
        },
        currentDateField(){
            return {type:'date',name:'_date'};
        },
        conditionValueType(state, getters){
            return condition=>{
                let fieldValueType;
                let field = getters.field(condition.fieldName);
                if(!field) return 'none';
                switch(field.type){
                    case 'file':
                    case 'photo':
                    case 'ssn':
                        return 'none';
                    case 'date':
                    case 'time':
                        fieldValueType = field.type;
                        break;
                    case 'checkboxes':
                    case 'radios':
                    case 'dropdown':
                        fieldValueType = 'select';
                        break;
                    default:
                        fieldValueType = 'text';
                }
                switch(condition.operation){
                    case 'between':
                        fieldValueType += '[2]';
                        break;
                    case 'in':
                    case 'checked':
                        fieldValueType += '[]';
                        break;
                }
                return fieldValueType;
            };
        },
    }
};