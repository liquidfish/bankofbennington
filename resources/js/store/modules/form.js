import Vue from "vue";

export default {
    namespaced: true,
    state: {
        fields: {},
    },
    mutations: {
        addField(state,field){
            Vue.set(state.fields,field.name,field);
        },
        setField(state,{name,value}){
            Vue.set(state.fields[name],'value',value);
        },
    },
    actions: {
    },
    getters: {
        field(state){
            return name=>state.fields[name];
        }
    }
}