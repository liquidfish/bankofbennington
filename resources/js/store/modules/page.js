
import { SnackbarProgrammatic as Snackbar } from 'buefy';
import Vue from 'vue';

let quickKey = ()=>Date.now()+'_'+_.uniqueId();
let vAssign = (destination, source)=>_.each(source,(value,key)=>{
    Vue.set(destination,key,value)
});

let axiosPatch = (path, data, then, thenMessage, catchMessage, commit)=>{
    axios.patch(path, data)
        .then(response => {
            then(response);
            Snackbar.open({
                message: thenMessage,
                position: 'is-bottom',
                indefinite: false,
            });
        }).catch(function (error) {
        // handle error
        console.log(error);
        Snackbar.open({
            message: catchMessage+': '+error,
            type: 'is-danger',
            position: 'is-top',
            indefinite: false,
        });
    })
        .finally(function () {
            // always executed
            commit('sync',{saving:false});
        })
};


export default {
    namespaced: true,
    state: {
        eventBus: new Vue(),
        initializing: false,
        initialized: false,
        saving: false,
        reload: false,
        page: {
            availableComponents: [],
            availableLayouts: [],
            componentTree: [],
            currentComponents: [],
            version: {},
        },
        editing: false,
        settingsOpen: false,
        settingsTab: 'settings',
        settings: {
            title: null,
            published: null,
            redirect_url: null,
            meta_tags: [
                {name: null, value: null, key: null, property: null}
            ],
        },
    },
    mutations: {
        sync(state,sync){
            _.assign(state, _.pick(sync, ['initializing','initialized','saving','reload']));
        },
        editing(state, editing){
            if(state.editing !== editing){
                state.editing = editing;
                state.eventBus.$emit('editingChanged');
            }
        },
        load(state, page){
            state.page = page;
        },
        loadSettings(state, page){
            state.settings = {
                nav_title: page.nav_title,
                title: page.version.title,
                published: page.published,
                redirect_url: page.redirect_url,
                meta_tags: _.map(page.version.meta_tags, tag=>({...tag,key:quickKey()})),
                header_tags: page.version.header_tags,
                footer_tags: page.version.footer_tags,
            };
        },
        loadAvailable(state, {availableLayouts,availableComponents}){
            _.assign(state.page,{availableLayouts,availableComponents});
        },
        loadComponents(state, currentComponents){
            state.page.currentComponents = _.map(currentComponents,c=>({...c,changed:false,originalData:_.cloneDeep(c.data),editing:{}}));
        },
        loadComponentTree(state, componentTree){
            state.page.componentTree = componentTree;
        },
        componentEditing(state, component){
            let currentComponent = _.find(state.page.currentComponents,{id:component.id});
            if(!currentComponent.editing) vAssign(currentComponent,{editing:{}});
            vAssign(currentComponent.editing,component.editing);
        },
        loadVersions(state, versions){
            state.page.versions = versions;
        },
        updateSettings(state, settings){
            _.each(settings, (value,key)=>state.settings[key] = value)
        },
        updateMetaTagSetting(state, {tag,name,value,property}){
            let settingTag = _.find(state.settings.meta_tags,tag);
            if(name !== undefined) settingTag.name = name;
            if(value !== undefined) settingTag.value = value;
            if(property !== undefined) settingTag.property = property;
        },
        removeMetaTag(state, tag){
            let index = _.findIndex(state.settings.meta_tags, tag);
            state.settings.meta_tags.splice(index, 1);
        },
        addMetaTag(state){
            let key = quickKey();
            console.log(key);
            state.settings.meta_tags.push({name:'',value:'',property:'',key});
        },
        addComponent(state, {template,parent}){
            state.page.componentTree.push({
                id: quickKey(),
                ..._.pick(template,['name','displayName','bowls']),
                ...parent,
                order: _.filter(state.page.componentTree,parent).length
            });
        },
        removeComponent(state,component){
            let index = _.findIndex(state.page.componentTree,_.pick(component,'id'));
            state.page.componentTree.splice(index,1);
            _.each(
                _.filter(state.page.componentTree,_.pick(component,['parent_component_id','bowl'])),
                c=>{if(c.order > component.order) c.order--;}
            );
        },
        moveComponent(state,{component,location}){
            _.assign(component,_.pick(location,['parent_component_id','bowl','order']));
        },
        settingsTray(state, tray){
            _.assign(state, _.pick(tray, ['settingsOpen','settingsTab']));
            let mainNavClassList = document.getElementById('larafish-main-nav').classList;
            if(state.settingsOpen){
                mainNavClassList.add('is-active');
            } else {
                mainNavClassList.remove('is-active');
            }
        },
        loadComponent(state, component){
            let index = _.findIndex(state.page.currentComponents,{id:component.id});
            state.page.currentComponents.splice(index, 1, {...component,changed:false,originalData:_.cloneDeep(component.data)});
        },
        componentData(state, component){
            let currentComponent = _.find(state.page.currentComponents,{id:component.id});
            if(!_.isEqual(component.data, currentComponent.data)){
                vAssign(currentComponent.data,component.data);
                currentComponent.changed = !_.isEqual(currentComponent.data, currentComponent.originalData);
            }
        },
        swapLayout(state, {newLayout,oldLayout}){
            _.assign(
                _.find(state.page.componentTree,_.pick(oldLayout,'id')),
                _.find(state.page.availableLayouts,{name:newLayout})
            );
        },
    },
    actions: {
        load({commit,getters},versionId){
            commit('sync',{initializing:true});
            // let queryPageVersion = (function(){
            //     var regex = new RegExp('[\\?&]page_version=([^&#]*)');
            //     var results = regex.exec(location.search);
            //     if(results === null) return '';
            //     else return '?page_version='+results[1];
            // })();
            // axios.get('/admin/pages'+getters.pagePath+'-data.json'+queryPageVersion)
            axios.get('/admin/page-versions/'+versionId+'/data.json')
                .then(response => {
                    commit('load',response.data);
                    commit('loadSettings',response.data);
                    commit('loadComponents',response.data.currentComponents);
                    commit('sync',{initializing: false, initialized: true});
                });
        },
        saveSettings({commit,state,getters}){
            commit('sync',{saving:true});

            let saveModel = _.pickBy(
                _.pick(state.settings,['title','published','redirect_url','header_tags','footer_tags']),
                setting=>setting !== null
            );
            saveModel.meta_tags = _.map(
                _.filter(state.settings.meta_tags,tag=>tag.name || tag.value),
                tag=>_.pick(tag, ['name','value','property'])
            );
            axiosPatch(
                '/admin/page-versions/'+state.page.version.id+'/settings', saveModel,
                response=>{commit('loadSettings',response.data);commit('sync',{reload:true});},
                'Page settings saved!',
                'Error saving changes',
                commit
            );

        },
        saveComponentTree({commit,state,getters}){
            commit('sync',{saving:true});
            axiosPatch(
                '/admin/page-versions/'+state.page.version.id+'/component-tree',
                state.page.componentTree,
                response=>{commit('loadComponentTree',response.data);commit('sync',{reload:true});},
                'Components saved!',
                'Error saving components',
                commit
            );
        },
        savePage({commit,state}) {
            commit('sync', {saving: true});
            axiosPatch(
                '/admin/page-versions/'+state.page.version.id+'/components',
                {components:_.map(_.filter(state.page.currentComponents,'changed'),c=>_.pick(c,['id','data']))},
                response=>_.each(response.data,c=>commit('loadComponent',c)),
                'Page saved!',
                'Error saving page',
                commit
            );
        },
        saveComponent({commit,state,getters},componentId){
            commit('sync',{saving:true});
            axiosPatch(
                '/admin/page-components/'+componentId,
                _.pick(getters.component(componentId),['data']),
                response=>commit('loadComponent',response.data),
                'Component saved!',
                'Error saving component',
                commit
            );
        },
        newVersion({state,commit},version=null){
            commit('sync',{saving:true});
            let path = '/admin/pages/'+state.page.id+'/versions';
            if(version) path += '/'+version.id+'/duplicate';

            axios.post(path)
                .then(response=>{
                    if(response.data.versionId) {
                        window.location = window.location.origin + state.page.path + '?page_version='+response.data.versionId;
                    } else {
                        Snackbar.open({
                            message: 'Something went wrong creating version',
                            type: 'is-danger',
                            position: 'is-top',
                            indefinite: false,
                        });
                    }
                })
                .catch((error)=>{
                    // handle error
                    console.log(error);
                    Snackbar.open({
                        message: 'Failed to create new version: '+error,
                        type: 'is-danger',
                        position: 'is-top',
                        indefinite: false,
                    });
                    commit('sync',{saving:false})
                });
        },
        publishVersion({state,commit},version){
            commit('sync',{saving:true});
            axios.patch('/admin/pages/'+state.page.id+'/versions/'+version.id+'/publish')
                .then(response=>{
                    commit('loadVersions',response.data);
                })
                .catch((error)=>{
                    // handle error
                    console.log(error);
                    Snackbar.open({
                        message: 'Failed to publish version: '+error,
                        type: 'is-danger',
                        position: 'is-top',
                        indefinite: false,
                    });
                })
                .finally(()=>commit('sync',{saving:false}));
        },
        requestPublishVersion({state,commit},version){
            commit('sync',{saving:true});
            axios.post('/admin/pages/'+state.page.id+'/versions/'+version.id+'/publish/request')
                .then(response=>{
                    let versions = state.page.versions;
                    _.each(_.filter(versions,{canCancelRequest:true}),
                        version=>_.assign(version,{publishingRequested:false,canCancelRequest:false}));
                    _.assign(_.find(versions,{id:version.id}),{publishingRequested:true,canCancelRequest:true});
                    commit('loadVersions',versions);
                })
                .catch((error)=>{
                    // handle error
                    console.log(error);
                    Snackbar.open({
                        message: 'Failed to request publishing: '+error,
                        type: 'is-danger',
                        position: 'is-top',
                        indefinite: false,
                    });
                })
                .finally(()=>commit('sync',{saving:false}));
        },
        cancelPublishRequest({state,commit},version){
            commit('sync',{saving:true});
            axios.delete('/admin/pages/'+state.page.id+'/versions/'+version.id+'/publish/request')
                .then(response=>{
                    let versions = state.page.versions;
                    _.assign(_.find(versions,{id:version.id}),{publishingRequested:false,canCancelRequest:false});
                    commit('loadVersions',versions);
                })
                .catch((error)=>{
                    // handle error
                    console.log(error);
                    Snackbar.open({
                        message: 'Failed to request publishing: '+error,
                        type: 'is-danger',
                        position: 'is-top',
                        indefinite: false,
                    });
                })
                .finally(()=>commit('sync',{saving:false}));
        },
        deleteVersion({state,commit},version){
            commit('sync',{saving:true});
            axios.delete('/admin/pages/'+state.page.id+'/versions/'+version.id)
                .then(response=>{
                    if(version.id === state.page.version.id){
                        window.location = window.location.origin + state.page.path;
                    } else {
                        commit('loadVersions',response.data);
                    }
                })
                .catch((error)=> {
                    // handle error
                    console.log(error);
                    Snackbar.open({
                        message: 'Failed to delete version: ' + error,
                        type: 'is-danger',
                        position: 'is-top',
                        indefinite: false,
                    })
                })
                .finally(()=>commit('sync',{saving:false}));
        },
        refreshComponentCache({commit}){
            commit('sync',{saving:true});
            axios.get('/admin/page-components')
                .then(response=>{
                    commit('loadAvailable',response.data);
                })
                .catch((error)=> {
                    // handle error
                    console.log(error);
                    Snackbar.open({
                        message: 'Failed to refresh available layouts and components',
                        type: 'is-danger',
                        position: 'is-top',
                        indefinite: false,
                    })
                })
                .finally(()=>commit('sync',{saving:false}));
        },
        dropComponentAtLocation({state,commit,getters},{component,location}){
            let movingComponent = getters.treeComponent(component.id);
            let sourceBowl = getters.treeBowl(movingComponent);
            let order = movingComponent.order;

            _.each(sourceBowl.slice(movingComponent.order+1),otherComponent=>commit('moveComponent',{component:otherComponent,location:{order:order++}}));
            let destinationBowl;
            if(movingComponent.parent_component_id !== location.parent_component_id || movingComponent.bowl !== location.bowl){
                // Moving from one bowl to another.
                destinationBowl = getters.treeBowl(location);
            } else {
                // Moving within the same bowl.
                destinationBowl = sourceBowl;
                destinationBowl.splice(_.findIndex(sourceBowl,movingComponent),1);
                if(location.order > movingComponent.order) location.order--;
            }
            commit('moveComponent',{component:movingComponent,location});
            order = component.order + 1;
            _.each(destinationBowl.slice(location.order),otherComponent=>commit('moveComponent',{component:otherComponent,location:{order:order++}}));
        },

    },
    getters: {
        ready(state){
            return state.initialized;
        },
        saving(state){
            return state.saving;
        },
        reload(state){
            return state.reload;
        },
        editing(state){
            return state.editing;
        },
        changed(state){
            return _.some(state.page.currentComponents,'changed');
        },
        pagePath(state, getters){
            let path = getters.ready ? state.page.path : window.location.pathname;
            return path === '/' ? '/%2F' : path; // handling for home page
        },

        component(state){
            return id => _.find(state.page.currentComponents,{id});
        },
        treeComponent(state){
            return id=>_.find(state.page.componentTree,{id});
        },
        treeBowl(state){
            return location=>_.sortBy(_.filter(state.page.componentTree,_.pick(location,['parent_component_id','bowl'])),'order');
        },
        canEditPage(state){return true;},//_.get(state,'page.permissions.canEditPage',false)},
        canEdit(state){return true;},//_.get(state, 'page.permissions.canEdit',false)},
        canVersion(state){return true;},//_.get(state, 'page.permissions.canVersion',false)},
        canPublish(state){return true;},//_.get(state, 'page.permissions.canPublish',false)},
        canEditScripts(state){return true;},//_.get(state, 'page.permissions.canEditScripts',false)},
    }
};
