import { Dialog } from 'buefy/dist/components/dialog'

let wrapNameInLi = collection=>_.join(_.map(collection,node=>'<li><span class="icon"><i class="mdi mdi-'+node.icon+' mdi-24px"></i></span><span>'+node.name+'</span></li>'),"\n");
let mimeTypeIcons = {
    image:'image'
};
let mimeTypeToIcon = mimeType=>mimeTypeIcons[_.split(mimeType,'/')[0]] || 'file';
let getPercent = (progress,total)=>parseInt( Math.round( ( progress * 100 ) / total ) );

export default {
    namespaced: true,
    state: {
        loading: false,
        permissions: {
            createIn:false,
            uploadTo:false,
            uploadImagesTo:false,
            uploadPublicTo:false,
        },
        name: '',
        path: '',
        breadcrumbs: ['Home'],
        folders: [],
        files: [],
        view: 'list',
        move: false,
        uploads: [],
        openControl: '',
        creatingFolder: false,
        renaming: false,
    },
    mutations: {
        setView(state, view){
            state.view = view;
        },
        setMove(state,move){
            state.move = move;
        },
        setLoading(state, loading){
            switch(typeof(loading)){
                case 'boolean':
                    state.loading = loading;
                    break;
                case 'object':
                    _.assign(state,_.pick(loading,['loading','creatingFolder','renaming']));
                    break;
            }
        },
        setFolder(state, data){
            _.assign(state,_.pick(data, ['name','path','permissions','breadcrumbs']));
            state.folders = _.map(data.folders,node=>_.assign(node,{
                checked:false,
                type:'folder',
                icon:'folder',
            }));
            state.files = _.map(data.files,node=>_.assign(node,{
                checked:false,
                type:'file',
                icon:mimeTypeToIcon(node.mimeType),
            }));
        },
        updateFiles(state,files){
            _.each(files,file=>{
                let existingFile = _.find(state.files,{path:file.path});
                if(existingFile){
                    _.assign(existingFile,file);
                } else {
                    state.files.push(_.assign(file,{checked:false,type:'file',icon:mimeTypeToIcon(file.mimeType)}));
                }
            });
        },
        setOpenControl(state, control = ''){
            state.openControl = control;
        },
        check(state, {files=[],folders=[],checked}){
            _.each(files, file=>_.noop(file.checked = checked));
            _.each(folders, folder=>_.noop(folder.checked = checked));
            state.openControl = '';
        },
        addUploads(state,{files,visibility}){
            _.each(files,file=>{
                state.uploads.push({
                    id: Date.now()+''+Math.random()+'|'+state.path+'/'+file.name+'|'+file.size,
                    folder: {name:state.breadcrumbs.join('/'),path:state.path},
                    file,
                    inProgress:false,
                    progress: 0,
                    complete: false,
                    failed: false,
                    icon: mimeTypeToIcon(file.type),
                    visibility,
                });
            });
        },
        markUploadStatus(state,file){
            let uploadFile = _.find(state.uploads,{id:file.id});
            _.assign(uploadFile,_.pick(file,['inProgress','progress','complete','uploadedAs','failed']));
        },
        cancelUpload(state,file){
            state.uploads.splice(_.indexOf(state.uploads,{id:file.id}),1);
        },
        fileUploaded(state,{path,response}){
            if(path === state.path){
                state.files.push(_.assign(response,{
                    checked:false,
                    type:'file',
                    icon:mimeTypeToIcon(response.mimeType),
                }));
            }
        },
        clearUploads(state){
            if(!_.some(state.uploads,'inProgress')){
                state.uploads = [];
            }
        }
    },
    actions: {
        loadFolder({commit},folder){
            commit('setLoading',true);
            axios.get('/admin/file-manager/folder?folder='+folder)
                .then(response=>{
                    commit('setFolder',response.data);
                })
                .catch(console.log)
                .finally(()=>commit('setLoading',false));
        },
        init({dispatch}){
            dispatch('loadFolder','');
        },
        refresh({state,dispatch}){
            dispatch('loadFolder',state.path);
        },
        renameSelected({commit,state,getters},newName){
            if(getters.checked.length !== 1){
                Dialog.alert("Can't rename multiple (how did you get this?)");
                return;
            }
            commit('setLoading',{renaming:true});
            let folder = state.path;
            let nodePath = getters.checked[0];
            axios.post('/admin/file-manager/rename',{folder,oldName:nodePath.name,newName})
                .then(response=>{
                    if(state.path === folder){
                        commit('setFolder',response.data);
                        if(state.openControl === 'rename') commit('setOpenControl','');
                    }
                })
                .catch(console.log)
                .finally(()=>commit('setLoading',{renaming:false}));
        },
        createFolder({commit,state},name){
            commit('setLoading',{creatingFolder:true});
            let folder = state.path;
            axios.post('/admin/file-manager/folder',{folder,name})
                .then(response=>{
                    if(state.path === folder){
                        commit('setFolder',response.data);
                        if(state.openControl === 'createFolder') commit('setOpenControl','');
                    }
                })
                .catch(console.log)
                .finally(()=>commit('setLoading',{creatingFolder:false}));
        },
        deleteSelected({commit,state,getters}){
            let canDelete,canNotDelete;
            [canDelete,canNotDelete] = _.partition(getters.checked,node=>node.permissions.delete);

            let message = 'Are you sure you want to delete these files and/or folders?<ul>' + wrapNameInLi(canDelete) + '</ul>';
            if(canNotDelete.length){
                message += "<br>You can't delete these files and/or folders:<ul>" + wrapNameInLi(canNotDelete) + '</ul>';
            }
            if(canDelete.length){
                Dialog.confirm({
                    title: 'Confirm delete',
                    message,
                    confirmText: 'Delete',
                    type: 'is-warning',
                    hasIcon: true,
                    onConfirm: ()=>{
                        commit('setLoading',{loading:true});
                        let folder = state.path;
                        let files = _.map(_.filter(state.files,'checked'),'path');
                        let folders = _.map(_.filter(state.folders,'checked'),'path');
                        axios.post('/admin/file-manager/remove',{folder,files,folders})
                            .then(response=>{
                                if(state.path === folder){
                                    commit('setFolder',response.data);
                                }
                            })
                            .catch(console.log)
                            .finally(()=>commit('setLoading',{loading:false}));
                    }
                });
            }
        },
        startMove({commit,state,getters}){
            let canMove,canNotMove;
            [canMove,canNotMove] = _.partition(getters.checked,node=>node.permissions.move);
            if(canNotMove.length){
                let message = "You can't move these files and/or folders:<ul>" + wrapNameInLi(canNotMove) + '</ul>';
                if(canMove.length){
                    message += '<br>Do you still want to move these files and/or folders?'
                    Dialog.confirm({
                        title: "Can't move some files and/or folders",
                        message,
                        confirmText: 'Delete',
                        type: 'is-warning',
                        hasIcon: true,
                        onConfirm: ()=>console.log('Delete confirmed')
                    });
                } else {
                    Dialog.alert({
                        title: "Can't move any selected files and/or folders",
                        message,
                        type: 'is-danger',
                        hasIcon: true,
                    });
                }
            }
        },
        upload({commit,state,getters,dispatch},{files,visibility}){
            commit('addUploads',{files,visibility});
            dispatch('makeSureUploadsAreInProgress');
        },
        makeSureUploadsAreInProgress({getters,dispatch}){
            if(!getters.uploadsInProgress){
                dispatch('uploadNext');
            }
        },
        uploadNext({commit,state,dispatch}){
            let next = _.find(state.uploads,{inProgress:false,failed:false,complete:false});
            if(next){
                let formData = new FormData();
                formData.append('folder',next.folder.path);
                formData.append('visibility',next.visibility);
                formData.append('file',next.file);
                commit('markUploadStatus',{
                    id: next.id,
                    inProgress: true,
                });
                axios.post('/admin/file-manager/upload',formData,
                    {
                        headers:{'Content-Type':'multipart/form-data'},
                        onUploadProgress:progressEvent=>{
                            commit('markUploadStatus',{
                                id:next.id,
                                progress: getPercent(progressEvent.loaded,progressEvent.total),
                            });
                        }
                    })
                    .then(response=>{
                        commit('markUploadStatus',{
                            id:next.id,
                            inProgress:false,
                            progress:100,
                            complete:true,
                            uploadedAs:response.data.name===next.file.name?null:response.data.name,
                        });
                        commit('fileUploaded',{path:next.folder.path,response:response.data});
                    })
                    .catch(error=>{
                        console.log('Upload failed',error);
                        commit('markUploadStatus',{
                            id:next.id,
                            inProgress:false,
                            failed:true,
                        });
                    })
                    .finally(()=>{
                        dispatch('uploadNext');
                    })
                ;
            }
        },
        setVisibility({commit,state,getters},visibility){
            let canSetVisibility = _.filter(getters.checked,
                    node=>node.type === 'file' && node.visibility !== visibility &&
                        ({public:node.permissions.makePublic,private:node.permissions.move})[visibility]);
            if(canSetVisibility.length){
                commit('setLoading',{loading:true});
                let folder = state.path;
                let files = _.map(canSetVisibility,'path');
                axios.post('/admin/file-manager/visibility',{folder,files,visibility})
                    .then(response=>{
                        if(folder === state.path){
                            commit('updateFiles',response.data);
                        }
                    })
                    .catch(console.log)
                    .finally(()=>commit('setLoading',{loading:false}));
            }
        }
    },
    getters: {
        filesAndFolders(state){
            return [...state.folders,...state.files];
        },
        checked(state,getters){
            return _.filter(getters.filesAndFolders,'checked');
        },
        node(state,getters){
            return path=>_.find(getters.filesAndFolders,{path});
        },
        uploadFolders(state){
            return _.groupBy(state.uploads,file=>file.folder.path);
        },
        uploadsInProgress(state){
            return _.some(state.uploads,'inProgress');
        }
    }
};