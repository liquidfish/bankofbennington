export default {
    namespaced: true,
    state: {
        observer: null,
        elements: [],
        scroll: {
            position: null,
            monitor: null,
        },
        time: Date.now(),
    },
    mutations: {
        addElement(state,element){
            state.elements.push({element,size:{}});
        },
        removeElement(state,element){
            let index = _.findIndex(state.elements,{element});
            if(index > -1) state.elements.splice(_.findIndex(state.elements,{element}),1);
        },
        initialize(state,dispatch){
            state.observer = new ResizeObserver(entries=>dispatch('resizeObserved',entries));
        },
        updateSize(state,{element,size}){
            let el = _.find(state.elements,{element});
            if(el) el.size = size;
        },
        updateScroll(state,scroll){
            _.assign(state.scroll,_.pick(scroll,['position','monitor']));
        },
    },
    actions: {
        observe({commit,state,dispatch,getters},elements){
            if(!Array.isArray(elements)) elements = [elements];
            if(!state.observer) {
                commit('initialize',dispatch);
            }
            _.each(elements,element=>{
                if(!getters.elementWrapper(element)){
                    commit('addElement',element);
                    state.observer.observe(element);
                }
            });
        },
        unobserve({commit,state,getters},elements){
            elements = _.arrayWrap(elements);
            _.each(elements,element=>{
                if(getters.elementWrapper(element)){
                    commit('removeElement',element);
                    state.observer.unobserve(element);
                }
            });
        },
        resizeObserved({commit},entries){
            _.each(entries,entry=>{
                commit('updateSize',{element:entry.target,size:entry.contentRect});
            });
        },
        startScrollMonitor({state,dispatch,commit}){
            if(state.scroll.monitor === null){
                commit('updateScroll',{monitor:{}});
                dispatch('updateScrollValue');
            }
        },
        updateScrollValue({state,commit,dispatch}){

            let position = window.pageYOffset;
            if(state.scroll.position !== position){
                commit('updateScroll',{position});
            }
            state.scroll.monitor.animationFrame = window.requestAnimationFrame(()=>dispatch('updateScrollValue'));
        },
    },
    getters: {
        elementWrapper(state){
            return element=>_.find(state.elements,{element});
        },
        elementSize(state, getters){
            return element=>(getters.elementWrapper(element)||{}).size;
        },
        navSize(state,getters){
            return getters.elementSize(state.primaryNav);
        }
    },
};