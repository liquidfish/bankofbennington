import Vue from 'vue';
import Vuex from 'vuex';
import FormStore from '@/store/modules/form';

Vue.use(Vuex);
let conditionalForms = [];
window.conditionalForms = conditionalForms;

let files = require.context('../components/forms' , true, /\.vue$/i);
files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));

Array.from(document.querySelectorAll('.lf-form[data-conditional]'))
    .forEach(el=>{
        conditionalForms.push(el.vue = new Vue({
            el,
            store:new Vuex.Store(FormStore),
        }));
    })
;