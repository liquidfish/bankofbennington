import flatpickr from 'flatpickr';
// document.addEventListener('DOMContentLoaded', e => {
    flatpickr(".lf-forms-date-field",{
        altInput: true,
        altFormat: "F j, Y",
        dateFormat: "Y-m-d",
        wrap: true,
    });
    flatpickr(".lf-forms-time-field",{
        enableTime: true,
        noCalendar: true,
        dateFormat: "h:i K",
    });
// });