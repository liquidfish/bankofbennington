/*
* Page Editor Settings
* */

import MediumEditor from 'medium-editor';
import ToolbarStates from './extensions/ToolbarStates';
import ButtonCluster from './extensions/ButtonCluster';
import ColorPicker from './extensions/ColorPicker';

const mdiIcon = icon => '<span class="icon"><i class="mdi mdi-'+icon+' mdi-24px"></i></span>';
let imageOptions = MediumEditor.extensions.button.extend({
    name:'imageOptions',
    contentDefault: mdiIcon('image-frame'),
    // handleClick:()=>this.$emit('openImageOptions'),
});
let paragraph = MediumEditor.extensions.button.extend({
    name:'paragraph',
    contentDefault: mdiIcon('format-paragraph'),
    action: 'append-p',
    title: 'Paragraph'
});

export const Settings = {
    toolbar: {
        allowMultiParagraphSelection: true,
        // buttonLabels: 'fontawesome',
        buttons: [
            {
                name: 'bold',
                contentDefault: mdiIcon('format-bold'),
            },
            {
                name: 'italic',
                contentDefault: mdiIcon('format-italic'),
            },
            {
                name: 'underline',
                contentDefault: mdiIcon('format-underline'),
            },
            {
                name: 'strikethrough',
                contentDefault: mdiIcon('format-strikethrough-variant'),
            },
            {
                name: 'subscript',
                contentDefault: mdiIcon('format-subscript'),
            },
            {
                name: 'superscript',
                contentDefault: mdiIcon('format-superscript'),
            },
            {
                name: 'anchor',
                contentDefault: mdiIcon('anchor'),
                customClassOption: 'button',
                targetCheckbox: true,
            },
            {
                name: 'quote',
                contentDefault: mdiIcon('format-quote-close'),
            },
            {
                name:'quoteCluster',
            },
            {
                name: 'orderedlist',
                contentDefault: mdiIcon('format-list-numbered'),
            },
            {
                name: 'unorderedlist',
                contentDefault: mdiIcon('format-list-bulleted'),
            },
            {
                name: 'indent',
                contentDefault: mdiIcon('format-indent-increase'),
            },
            {
                name: 'outdent',
                contentDefault: mdiIcon('format-indent-decrease'),
            },
            {
                name: 'justifyLeft',
                contentDefault: mdiIcon('format-align-left'),
            },
            {
                name: 'justifyCenter',
                contentDefault: mdiIcon('format-align-center'),
            },
            {
                name: 'justifyRight',
                contentDefault: mdiIcon('format-align-right'),
            },
            {
                name: 'justifyFull',
                contentDefault: mdiIcon('format-align-justify'),
            },
            {
                name:'justifyCluster',
            },
            {
                name: 'h1',
                contentDefault: mdiIcon('format-header-1'),
            },
            {
                name: 'h2',
                contentDefault: mdiIcon('format-header-2'),
            },
            {
                name: 'h3',
                contentDefault: mdiIcon('format-header-3'),
            },
            {
                name: 'h4',
                contentDefault: mdiIcon('format-header-4'),
            },
            {
                name: 'h5',
                contentDefault: mdiIcon('format-header-5'),
            },
            {
                name: 'h6',
                contentDefault: mdiIcon('format-header-6'),
            },
            {
                name: 'p',
            },
            // {
            //     name: 'p',
            //     action: 'append-p',
            //     aria: 'paragraph',
            //     tagNames: ['p'],
            //     contentDefault: mdiIcon('format-paragraph'),
            // },
            {
                name: 'headerCluster',
            },
            {
                name: 'imageOptions',
                // contentDefault: mdiIcon('image-frame')
            },
            {
                name: 'removeFormat',
                contentDefault: mdiIcon('format-clear'),
            },
            {
                name: 'colorPicker',
                contentDefault: mdiIcon('brush')
            }
        ],
        diffLeft: 0,
        diffTop: -10,
        firstButtonClass: 'medium-editor-button-first',
        lastButtonClass: 'medium-editor-button-last',
        relativeContainer: null,
        standardizeSelectionStart: false,
        static: false,
        align: 'center',
        sticky: false,
        updateOnEmptySelection: false,

    },
    extensions: {
        toolbar_states:new ToolbarStates({
            hideClass: 'is-hidden',
            states: {
                default: {
                    buttons: ['bold','italic','underline','strikethrough','subscript','superscript','anchor','quote',
                        'orderedlist','unorderedlist','indent','outdent','justifyLeft','justifyCenter','justifyRight','justifyFull',
                        'h1','h2','h3','h4','h5','h6','p','removeFormat','justifyCluster','headerCluster','quoteCluster','colorPicker'
                    ]
                },
                image: {
                    buttons: ['anchor','quote','indent','outdent','justifyLeft','justifyCenter','justifyRight','justifyFull','justifyCluster','removeFormat','imageOptions','quoteCluster'],
                    nodeNames: ['IMG']
                }
            }
        }),
        quoteCluster:new ButtonCluster({
            name:'quoteCluster',
            buttons:['quote','indent','outdent'],
        }),
        justifyCluster:new ButtonCluster({
            name:'justifyCluster',
            buttons:['justifyLeft','justifyCenter','justifyRight','justifyFull'],
        }),
        p: new paragraph({
            name:'p',
        }),
        headerCluster:new ButtonCluster({
            name:'headerCluster',
            buttons:['p','h1','h2','h3','h4','h5','h6'],
        }),
        colorPicker: new ColorPicker(),
        imageOptions // This get constructed in lfEditableArea
    }
};