import MediumEditor from 'medium-editor';
window.MediumEditor  = MediumEditor;
let classyElement = (nodeName,classList)=> _.tap(document.createElement(nodeName),
    el=>_.each(_.arrayWrap(classList),classy=>el.classList.add(classy))
);

let ButtonCluster = MediumEditor.Extension.extend({
    name: 'buttonCluster',
    buttons: [],
    init: function(){
        let cluster = classyElement('div','dropdown');

        cluster.trigger = classyElement('button','dropdown-trigger');
        cluster.trigger.onclick=()=>cluster.classList.toggle('is-active');

        let menu = classyElement('div','dropdown-menu');
        menu.content = classyElement('div','dropdown-content');

        cluster.tray = classyElement('div','dropdown-item');

        cluster.appendChild(cluster.trigger);
        cluster.appendChild(menu);
        menu.appendChild(cluster.tray);

        menu.style.minWidth = '0';
        cluster.tray.style.padding = '0';
        this.cluster = cluster;
        this.subscribe('hideToolbar',()=>{
            cluster.classList.remove('is-active');
            this.initComplete = false;
        });
        this.subscribe('positionToolbar', ()=>this.initToolbar());
    },
    initToolbar(){
        if(!this.initComplete){
            this.toolbarButtons = _.filter(_.mapValues(_.keyBy(this.buttons),button=>this.base.getExtensionByName(button)));
            _.each(this.toolbarButtons,button=>{
                if(button.getButton){
                    this.cluster.tray.appendChild(button.getButton());
                }
            });
            _.each(this.base.extensions,ext=>{
                if(ext.getButton){
                    this.on(ext.getButton(),'click',click=>this.toolbarClick(click));
                }
            });
            this.initComplete = true;
        }
    },
    getButton: function(){
        if(this.initComplete){
            let refButton = _.find(this.toolbarButtons,button=>button.isActive());
            let isActive = Boolean(refButton);
            if(!isActive) refButton = _.first(this.toolbarButtons);
            this.cluster.trigger.innerHTML = refButton.getButton().innerHTML;
            if(isActive)this.cluster.trigger.classList.add('medium-editor-button-active');
            else this.cluster.trigger.classList.remove('medium-editor-button-active');
        }
        return this.cluster;
    },
    toolbarClick(event){
        if(!this.cluster.contains(event.target)){
            this.cluster.classList.remove('is-active');
        }
    }
});
export default ButtonCluster;