/**
 * Custom `color picker` extension
 */
import MediumEditor from 'medium-editor';
import Picker from 'vanilla-picker';
var ColorPicker = MediumEditor.Extension.extend({
    name: "colorPicker",

    init: function () {
        this.button = this.document.createElement('button');
        this.button.classList.add('medium-editor-action');
        this.button.classList.add('editor-color-picker');
        this.button.title = 'Text color';
        this.button.innerHTML = '<i class="mdi mdi-brush mdi-24px"></i>';
        this.on(this.button, 'click', this.handleClick.bind(this));
    },

    getButton: function () {
        return this.button;
    },

    handleClick: function (e) {
        e.preventDefault();
        e.stopPropagation();

        this.selectionState = this.base.exportSelection();

        // If no text selected, stop here.
        if (this.selectionState && (this.selectionState.end - this.selectionState.start === 0)) {
            return;
        }

        const picker = new Picker(this.document.querySelector(".medium-editor-toolbar-active .editor-color-picker").parentNode);
        picker.show();
        picker.onDone = function (color) {
            // Imports the start and end of what we selected in the content area
            this.base.importSelection(this.selectionState);

            // Modifies HTMl
            this.document.execCommand("styleWithCSS", false, "true");
            this.document.execCommand("foreColor", false, color.rgbaString);
            picker.destroy();
        }.bind(this)
    }
});
export default ColorPicker;