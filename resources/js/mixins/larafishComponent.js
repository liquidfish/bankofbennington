import pageStore from '@/store/modules/page';
export default {
    props: {
        componentId: Number,
    },
    computed: {
        component(){
            return this.$store.getters['page/component'](this.componentId);
        },
        ready(){
            return !!this.component;
        },
        data(){
            return this.ready ? this.component.data : null;
        },
        bowls(){
            return this.ready ? this.component.bowls : null;
        },
        editing(){
            return this.ready && this.$store.getters['page/editing'];
        },
        changed(){
            return this.ready && (this.component.changed || false);
        },
        eventBus(){
            return this.$store.state.page.eventBus;
        }
    },
    methods: {
        update(data){
            this.$store.commit('page/componentData',{id:this.component.id,data});
        },
        save(){
            this.$store.dispatch('page/saveComponent',this.component.id);
        },
        reset(){
            this.$store.commit('page/componentData',{id:this.component.id,data:this.component.originalData});
        },
        edit(editing) {
            this.$store.commit('page/componentEditing', {id: this.component.id, editing});
        },
    },
    beforeCreate() {
        this.$store.state.page || this.$store.registerModule('page',pageStore);
    }
}