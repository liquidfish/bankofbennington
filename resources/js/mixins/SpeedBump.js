export default {
    methods: {
        initializeSpeedBump(whitelist) {
            if(!document.querySelector('#larafish_admin')) {
                // Get all links
                const links = document.querySelectorAll('a[href]');
                const localDomains =
                    [
                        ...whitelist,
                        window.appUrl.split('/').pop(),
                        '//'
                    ];

                // Check if link is external
                links.forEach((link) => {
                    let domainMatch = link.href.match(/^http[s]?:\/\/([^\/?#]+)/);
                    if (domainMatch && !localDomains.includes(domainMatch[1])) {
                        link.addEventListener('click', this.openModal);
                    }
                });
            }
        }
    }
}