export default {
    methods:{
        id(prepend = null){
            return prepend ? _.join([prepend,this._uid],'-') : this._uid;
        },
    }
}