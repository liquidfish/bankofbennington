export default {
    created () {
        if(this.$options.listeners){
            this.$options.listeners = this.$options.listeners.bind(this)();
            Object.keys(this.$options.listeners).forEach(key=>this.$eventBus.$on(key, this.$options.listeners[key]));
        }
    },
    beforeDestroy(){
        if(this.$options.listeners ){
            Object.keys(this.$options.listeners).forEach(key=>this.$eventBus.$off(key, this.$options.listeners[key]));
        }
    },
}