import FormStore from '@/store/modules/form';

export default {
    props: {
        dataField: String,
        dataOldValue: String
    },
    data(){
        return {
            name:null,
            startingValue:this.dataOldValue||null,
        }
    },
    mounted(){
        let dataField = JSON.parse(this.dataField);
        this.name = dataField.name;
        this.$store.commit('form/addField',dataField);
        this.value = this.startingValue;
    },
    beforeCreate(){
        if(!this.$store.state.form){
            this.$store.registerModule('form',FormStore);
        }
    },
    computed:{
        field(){
            return this.$store.getters['form/field'](this.name);
        },
        required(){
            return this.field.required;
        },
        value: {
            get(){
                return this.field ? this.field.value : this.startingValue;
            },
            set(value){
                this.$store.commit('form/setField',{name:this.name,value});
            }
        },
    }
}