import SwiperCore, { Navigation, Pagination, Autoplay } from 'swiper/core';
import 'swiper/swiper-bundle.css';
SwiperCore.use([Navigation, Pagination, Autoplay]);
export default class SwiperSlider {
    constructor() {
        this.initSlider();
    }

    initSlider() {
        const swiper = new SwiperCore('.swiper-container', {
            loop: true,
            pagination: {
                el: '.swiper-pagination',
                type: 'bullets',
                clickable: true,
            },
            autoplay: {
                delay: 5000,
            },
            slidesPerView: 1
        });
    }
}
