export default class Navigation {
    constructor() {
        const hamburgerBtn = document.querySelector('.menu-btn');
        hamburgerBtn.addEventListener('click', () => {
            document.querySelector('.navigation-menu').classList.toggle('hidden');
        });
    }
}