module.exports = {
  purge: [
    './resources/**/*.blade.php',
    './resources/**/*.js',
    './resources/**/*.vue',
  ],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      lineHeight: {
        '15': '3.75rem',
      },
      fontSize: {
        '2.5xl': '1.75rem',
        '3.5xl': '2rem',
        '7xl': '4.5rem'
      },
      fontFamily: {
        'titillium': ['Titillium Web', 'sans-serif']
      },
      height: {
        'slider': '500px'
      },
      minHeight: (theme) => ({
        ...theme('spacing'),
      }),
      maxWidth: {
        '1-5xl': '1440px',
      },
      colors: {
        bank: {
          'dark-green': '#0E7053',
          'mid-green': '#66915A',
          'light-green': '#68AA67',
          yellow: '#F1CB48',
          dark: '#333333',
          'base-mid': '#6E7981',
          'base-light': '#F5F5F5'
        },
        primary: {
          DEFAULT: '#00AEEF',
          focus: '#1989B2',
          content: '#FFFFFF'
        },
        secondary: {
          DEFAULT: '#0065A1',
          focus: '#0A4365',
          content: '#FFFFFF'
        },
        accent: {
          DEFAULT: '#FFD910',
          focus: '#EBBA0E',
          content: '#001A28'
        },
        neutral: {
          DEFAULT: '#285066',
          focus: '#001A28',
          content: '#FFFFFF'
        },
        base: {
          DEFAULT: '#001A28',
          300: '#97ABB4',
          200: '#EDF0F2',
          100: '#FFFFFF',
        },
        state: {
          info: '#0D79F8',
          success: '#0DA563',
          warning: '#F59B00',
          error: '#E5443D'
        }
      }
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
