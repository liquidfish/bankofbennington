<?php

return [
    'tank_key'=>env('TANK_KEY'),
    'default_file_visibility'=>env('DEFAULT_FILE_VISIBILITY','public'),
    'recaptcha'=>[
        'key'=>env('RECAPTCHA_KEY'),
        'secret'=>env('RECAPTCHA_SECRET')
    ],
    'component_root'=>env('COMPONENT_ROOT','App\\Components'),
    'remote_file_source'=>env('LARAFISH_REMOTE_FILE_SOURCE'),
    'include_components'=>[
        'Larafish::ContentArea',
        'Larafish::DefaultLayout',
        'Larafish::FormDisplay',
        'Larafish::TwoColumn',
    ],
    'allow_post_sitemap'=>true,
];